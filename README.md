RaUt is a mod designed to work well with bigger mods but to not depend on them (balanced for vanilla). It adds various
items in a vanilla+ style. For example, armor between iron and diamond and an overworld-equivalent to netherite. There
is also a backpack, recipes for horse armor and chainmail, Access to more resources in creative menus (eg Command
Blocks, Barriers), Cotton as a way to farm string or a block you can place in mid-air. All of this is very configurable
and every component can independently be tweaked or disabled.\

![Items](https://gitlab.com/jfmods/RaUt2/-/raw/master/Items.png?inline=false)
\
These Items are (in order):

- Aquilorite Armor, crafted from Aquilorite, slightly less powerful than netherite
- Aquilorite Ore / Blocks: The ore spawns in the overwold, the first block is pass-through, the second one behaves like
  ice
- Aquilorite Gem: This is dropped from Aquilorite Ore and used for crafting the more powerful items in this mod,
  replaced with diamond if disabled in config
- The Aquilorite Paxel is the only tool craftable from Aquilorite Gems and slightly better than diamond tools
- This bullet/gun behave like a faster bow & arrow that is more expensive to craft and doesn't allow picking up
  projectiles
- The cotton seeds drop strings. You can find them by mining grass
- The crystal seeds can only be found in chests. Their drop grants random effects and can be used to craft golden
  apples (configurable)
- The boost block rapidly accelerates the player. This might conflict with the maximum speed setting on servers
- This is used to craft chainmail armor
- This block can be placed mid-air
- Glass shards drop from glass and can be used to get some glass back after you break blocks
- The ender plant is used to grow ender pearls
- The cobblestone generator generates a bit of cobblestone automatically. You have to right-click the block to receive
  the generated stone
- The block breaker breaks blocks when powered on, use this with a redstone clock
- These four items are processed ores. You can get them by crafting ores together with an anvil. Slightly overpowered
- The steel tools are slightly better than iron tools but require additional effort to craft (namely: steel)
- Unprocessed steel is crafted from coal and iron and can be smelted to get steel ingots
- These items behave like iron
- Steel armor. Once again slightly better than iron
- The travellers ring grants additional movement speed and allows to step up whole blocks (useful for running up
  mountains)
- The angels ring grants creative flight
- The builders ring increases the range for building
- These behave like the aquilorite paxel but their properties are adjusted to their respective materials
- The staff of time can be used to speed up blocks like furnaces or machines

\
You can also block items from backpacks and shulker boxes by adding them to the tag: raut:shulker_boxes_illegal\
\
If RaUt crashes with a NullPointerException after an update it might be due to the config being invalid. Look in the
config file and remove all entries that say: "something": null
