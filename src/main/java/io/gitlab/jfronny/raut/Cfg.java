package io.gitlab.jfronny.raut;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry.BoundedDiscrete;
import me.shedaniel.autoconfig.annotation.ConfigEntry.Category;
import me.shedaniel.autoconfig.annotation.ConfigEntry.Gui.CollapsibleObject;
import me.shedaniel.autoconfig.annotation.ConfigEntry.Gui.RequiresRestart;
import me.shedaniel.autoconfig.annotation.ConfigEntry.Gui.TransitiveObject;
import me.shedaniel.cloth.clothconfig.shadowed.blue.endless.jankson.Comment;
import net.fabricmc.loader.api.FabricLoader;
import org.lwjgl.glfw.GLFW;

@Config(name = "RaUt2")
public class Cfg implements ConfigData {
    @Comment(value = "Miscellaneous content")
    @Category("Misc")
    @TransitiveObject
    public MiscModule misc = new MiscModule();

    @Comment(value = "Adds steel as a resource in between iron and diamonds. Contains processing, ingots, nuggets, blocks, armor and tools")
    @Category("Steel")
    @TransitiveObject
    public SteelModule steel = new SteelModule();

    @Comment(value = "A new, powerful ore. Includes the ore, the aquilorite gem, two full blocks and armor")
    @Category("Aquilorite")
    @TransitiveObject
    public AquiloriteModule aquilorite = new AquiloriteModule();

    @Comment(value = "Adds a new plant that can be used to farm string")
    @Category("Cotton")
    @TransitiveObject
    public CottonModule cotton = new CottonModule();

    @Comment(value = "Adds a new plant that can be used to get random potion effects")
    @Category("CrystalPlant")
    @TransitiveObject
    public CrystalPlantModule crystalPlant = new CrystalPlantModule();

    @Comment(value = "Adds some trinkets")
    @Category("Trinkets")
    @TransitiveObject
    public TrinketsModule trinkets = new TrinketsModule();

    @Comment(value = "Adds overpowered creative-only items. Only useful for debugging")
    @Category("Debug")
    @TransitiveObject
    public DebugModule debug = new DebugModule();

    @Comment("Changes how disabled items will behave (delete (unsafe) vs disable + hide (safe))")
    @RequiresRestart
    public Boolean safeDisable = true;

    @RequiresRestart
    @Comment("Enable REI compatibility")
    public Boolean reiCompat = true;

    @Comment("Enables toggling illegal items in shulkers")
    @RequiresRestart
    public boolean shulkerIllegal = true;

    public static class MiscModule {
        @Comment("Ore processing")
        @RequiresRestart
        public Boolean oreProcessing = true;
        @Comment("A recipe for chainmail armor")
        public Boolean chainmailRecipe = true;
        @Comment("A recipe for horse armor")
        public Boolean horseArmorRecipe = true;
        @Comment("Grass drops melon/pumpkin/beetroot seeds")
        @CollapsibleObject
        public MoreSeeds moreSeeds = new MoreSeeds();
        @Comment("An angel block (a block you can place in mid-air)")
        @CollapsibleObject
        public AngelBlock angelBlock = new AngelBlock(/*enabled*/);
        @Comment("A block that speeds you up drastically")
        @CollapsibleObject
        public Boost boostBlock = new Boost();
        @Comment("A crafting-item, used by other modules if available")
        @RequiresRestart
        public Boolean chainPlate = true;
        @Comment("A recipe for diamond armor that uses chainmail as a base (like aquilorite)")
        public Boolean betterDiamondRecipe = true;
        @Comment("Adds some items to the creative menus that are hidden by default (eg command blocks)")
        @RequiresRestart
        public Boolean extraCreativeItems = true;
        @Comment("Glass drops some shards so you don't loose all of it")
        @RequiresRestart
        public Boolean glassShards = true;
        @Comment("A plant to farm ender pearls")
        @CollapsibleObject
        public EndPlant endPlant = new EndPlant();
        @Comment("A simple cobblestone generator")
        @CollapsibleObject
        public CobbleGenerator cobbleGen = new CobbleGenerator();
        @Comment("A simple block breaker")
        @CollapsibleObject
        public BlockBreaker blockBreaker = new BlockBreaker();
        @Comment("An item to accelerate various processes (note: use the tag \"raut:staff_of_time_illegal\" to prevent usage on specific blocks)")
        @CollapsibleObject
        public StaffOfTime staffOfTime = new StaffOfTime();
        @Comment(value = "Adds paxels for vanilla resources. Disable if another mod does that already.")
        public Boolean vanillaPaxels = !FabricLoader.getInstance().isModLoaded("variablepaxels") && !FabricLoader.getInstance().isModLoaded("mattocks");

        public static class AngelBlock {
            //public AngelBlock(boolean enabled) {
            //    this.enabled = true;
            //}
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("The distance to place the block at when placed in mid-air")
            @BoundedDiscrete(max = 10, min = 2)
            public Integer placeDistance = 3;
        }

        public static class CobbleGenerator {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("The maximum amount of cobblestone that can be contained in one block")
            public int maxCobble = 512;
            @Comment("The delay before the first stone after the block is placed")
            public int initialDelay = 10;
            @Comment("The delay for cobblestone generation after the first")
            public int runDelay = 90;
            @Comment("Block hardness")
            @RequiresRestart
            public int hardness = 2;
            @Comment("Block resistance")
            @RequiresRestart
            public int resistance = 6;
        }

        public static class BlockBreaker {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("Block hardness")
            @RequiresRestart
            public int hardness = 2;
            @Comment("Block resistance")
            @RequiresRestart
            public int resistance = 6;
        }

        public static class Boost {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("This should technically be lower than 1 but setting it this high is what allows this block to function")
            @RequiresRestart
            public int slipperiness = 10;
            @Comment("Block hardness")
            @RequiresRestart
            public int hardness = 3;
            @Comment("Block resistance")
            @RequiresRestart
            public int resistance = 3;
        }

        public static class MoreSeeds {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("Chance of seeds being dropped from grass")
            public float dropChance = 0.125f;
        }

        public static class EndPlant {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("Chance of seeds spawning in chests")
            public float chestChance = 0.1f;
        }

        public static class StaffOfTime {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("The number of times each update should be done per click")
            public int speed = 100;
            @Comment("The number to divide the speed by for RandomTick")
            public int randomTickDiv = 10;
            @Comment("The amount of times the staff can be used")
            @RequiresRestart
            public int maxDamage = 1000;
        }
    }

    public static class SteelModule {
        @RequiresRestart
        public Boolean enabled = true;
        @Comment("Steel nuggets")
        @RequiresRestart
        public Boolean nugget = true;
        @Comment("Steel blocks")
        @CollapsibleObject
        public Boolean block = true;
        @Comment("Steel Tools. Between iron and diamond")
        @RequiresRestart
        public Boolean tools = true;
        @Comment("Steel Armor. Between iron and diamond")
        @RequiresRestart
        @CollapsibleObject
        public Boolean armor = true;
    }

    public static class AquiloriteModule {
        @RequiresRestart
        public Boolean enabled = true;
        @Comment("A walk-through block")
        @CollapsibleObject
        public Boolean aquiloriteBlock = true;
        @Comment("Just another ore block")
        @CollapsibleObject
        public Boolean aquiloriteBlockHardened = true;
        @Comment("Armor similarly powered to netherite")
        @CollapsibleObject
        public AquiloriteArmor aquiloriteArmor = new AquiloriteArmor();
        @Comment("A powerful multitool")
        @CollapsibleObject
        public Paxel aquiloritePaxel = new Paxel();
        @Comment("Aquilorite gun")
        @CollapsibleObject
        public Gun gun = new Gun();

        public static class AquiloriteArmor {
            @RequiresRestart
            public Boolean enabled = true;
            @RequiresRestart
            @Comment("Makes wearers invincible")
            public Boolean overpowered = false;
        }

        public static class Gun {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("Makes the gun kill everything instantly")
            public Boolean overpowered = false;
        }

        public static class Paxel {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("Makes the paxel kill everything instantly")
            public Boolean overpoweredDamage = false;
        }
    }

    public static class CottonModule {
        @RequiresRestart
        public Boolean enabled = !FabricLoader.getInstance().isModLoaded("flax");
        public float dropChance = 0.042f;
    }

    public static class CrystalPlantModule {
        @RequiresRestart
        public Boolean enabled = true;
        @Comment("Allows to craft Gold Apples using Crystal Plants")
        public Boolean craftGApples = true;
        @Comment("Allows to craft Notch Apples from Gold Apples using Crystal Plants")
        public Boolean craftGApples2 = true;
        @Comment("Settings for the crystal consumable")
        @CollapsibleObject
        public Crystal crystal = new Crystal();
        @Comment("Chance of items to generate in a chest")
        public float chestChance = 0.1f;

        public static class Crystal {
            @RequiresRestart
            public int hunger = 1;
            @RequiresRestart
            public float saturationModifier = 0.1f;
            @Comment("The chance of negative effect. A percentage can be approximated by 1/this")
            public int negativeChanceInvert = 5;
            public int effectAmplifier = 1;
            public int effectDuration = 1200;
            public int cooldown = 10;
        }
    }

    public static class TrinketsModule {
        @RequiresRestart
        public Boolean enabled = FabricLoader.getInstance().isModLoaded("trinkets");
        @Comment("Ring that allows you to run faster/step higher")
        @CollapsibleObject
        public TravellersRing traveller_ring = new TravellersRing();
        @Comment("Ring that allows you to reach further")
        @RequiresRestart
        public Boolean angel_ring = true;
        @Comment("Ring that allows you to reach further")
        @CollapsibleObject
        public BuildersRing builders_ring = new BuildersRing();
        @Comment("Backpack to store items. Comes in three tiers")
        @CollapsibleObject
        public Backpack backpack = new Backpack();

        public static class Backpack {
            @RequiresRestart
            public Boolean enabled = true;
            @RequiresRestart
            public int defaultKey = GLFW.GLFW_KEY_B;
            public int smallRows = 3;
            public int mediumRows = 5;
            public int largeRows = 7;
        }

        public static class BuildersRing {
            @RequiresRestart
            public Boolean enabled = true;
            @RequiresRestart
            public float addedRange = 1.5f;
        }

        public static class TravellersRing {
            @RequiresRestart
            public Boolean enabled = true;
            @Comment("The additional height you can walk up 1 = 1 block")
            @RequiresRestart
            public float addedStepHeight = 0.5f;
            @Comment("The additional movement speed based on the base speed")
            @RequiresRestart
            public float movementSpeedFactor = 0.5f;
        }
    }

    public static class DebugModule {
        @RequiresRestart
        public Boolean enabled = false;
        @Comment("Item to easily clear large areas")
        @CollapsibleObject
        public DebugClear debugClear = new DebugClear();

        public static class DebugClear {
            @RequiresRestart
            public Boolean enabled = false;
            public int XRange = 10;
            public int YRange = 10;
            public int ZRange = 10;
        }
    }
}
