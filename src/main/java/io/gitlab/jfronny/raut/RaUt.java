package io.gitlab.jfronny.raut;

import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.modules.*;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.JanksonConfigSerializer;
import net.devtech.arrp.api.RRPCallback;
import net.devtech.arrp.api.RuntimeResourcePack;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

//TODO Note to self: I can yank some textures from https://github.com/malcolmriley/unused-textures
public class RaUt implements ModInitializer {
    public static final String MOD_ID = "raut";
    public static final String MOD_NAME = "RaUt2";
    public static final ArrayList<BaseModule> modules = new ArrayList<>();
    private static final Logger logger = LogManager.getFormatterLogger(MOD_ID);
    public static Cfg cfg;

    public static final RuntimeResourcePack DATA_PACK = RuntimeResourcePack.create(MOD_ID);

    public static void info(String msg) {
        logger.log(Level.INFO, "[" + MOD_NAME + "] " + msg);
    }

    public static void warn(String msg) {
        logger.log(Level.WARN, "[" + MOD_NAME + "] " + msg);
    }

    public static void debug(String msg) {
        if (FabricLoader.getInstance().isDevelopmentEnvironment())
            info(msg);
        else
            logger.log(Level.DEBUG, "[" + MOD_NAME + "] " + msg);
    }

    public static void loadCfg() {
        if (cfg == null) {
            AutoConfig.register(Cfg.class, JanksonConfigSerializer::new);
            cfg = AutoConfig.getConfigHolder(Cfg.class).getConfig();
        }
    }

    @Override
    public void onInitialize() {
        logger.info("Initializing");
        loadCfg();
        modules.add(new AquiloriteModule());
        modules.add(new CottonModule());
        modules.add(new CrystalPlantModule());
        modules.add(new DebugModule());
        modules.add(new MiscModule());
        modules.add(new OreProcessingModule());
        modules.add(new SteelModule());
        modules.add(new TrinketsModule());
        modules.add(new DataFixerModule());

        for (BaseModule module : modules) {
            module.init();
        }

        LootTableLoadingCallback.EVENT.register((resourceManager, lootManager, id, supplier, setter) -> {
            for (BaseModule module : modules) {
                module.onLootTableLoading(resourceManager, lootManager, id, supplier, setter);
            }
        });
        for (Item item : DepRegistry.disabledItems.values()) {
            RecipeUtil.removeRecipeFor(new ItemStack(item));
        }

        RRPCallback.AFTER_VANILLA.register(a -> a.add(DATA_PACK));
    }
}