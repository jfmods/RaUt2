package io.gitlab.jfronny.raut.integration;

import io.gitlab.jfronny.raut.util.DepRegistry;
import me.shedaniel.rei.api.client.plugins.REIClientPlugin;
import me.shedaniel.rei.api.client.registry.entry.EntryRegistry;
import me.shedaniel.rei.api.common.entry.EntryStack;
import me.shedaniel.rei.api.common.entry.type.VanillaEntryTypes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import static io.gitlab.jfronny.raut.RaUt.cfg;

//TODO test removal
public class REICompat implements REIClientPlugin {
    @Override
    public double getPriority() {
        return 1;
    }

    @Override
    public void registerEntries(EntryRegistry registry) {
        if (cfg.reiCompat) {
            recheckItemHiding(registry);
        }
    }

    public void recheckItemHiding(EntryRegistry entryRegistry) {
        for (Item disabledItem : DepRegistry.disabledItems.values()) {
            entryRegistry.removeEntryIf(entryStack -> entryStack.getType() == VanillaEntryTypes.ITEM && ((EntryStack<ItemStack>)entryStack).getValue().getItem() == disabledItem);
        }
    }
}
