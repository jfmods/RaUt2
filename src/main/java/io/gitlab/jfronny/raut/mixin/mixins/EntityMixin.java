package io.gitlab.jfronny.raut.mixin.mixins;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.modules.AquiloriteModule;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.concurrent.atomic.AtomicInteger;

@Mixin(Entity.class)
public class EntityMixin {
    @Inject(at = @At("HEAD"), method = "isInvulnerable()Z", cancellable = true)
    public void setInvulnerable(CallbackInfoReturnable<Boolean> info) {
        if (RaUt.cfg.aquilorite.aquiloriteArmor.overpowered && isWearingAquilorite()) {
            info.setReturnValue(true);
        }
    }

    @Inject(at = @At("HEAD"), method = "isInvulnerableTo(Lnet/minecraft/entity/damage/DamageSource;)Z", cancellable = true)
    public void setInvulnerable(DamageSource source, CallbackInfoReturnable<Boolean> info) {
        if (RaUt.cfg.aquilorite.aquiloriteArmor.overpowered && isWearingAquilorite()) {
            info.setReturnValue(true);
        }
    }

    @Inject(at = @At("HEAD"), method = "kill()V", cancellable = true)
    public void setKillable(CallbackInfo info) {
        if (RaUt.cfg.aquilorite.aquiloriteArmor.overpowered && isWearingAquilorite()) {
            info.cancel();
        }
    }

    private boolean isWearingAquilorite() {
        Entity entity = (Entity) (Object) this;
        AtomicInteger armorCount = new AtomicInteger();
        entity.getArmorItems().forEach(s -> {
            //TODO test
            if (AquiloriteModule.AQUILORITE_ARMOR_TAG.contains(s.getItem()))
                armorCount.getAndIncrement();
        });
        return armorCount.get() >= 4;
    }
}
