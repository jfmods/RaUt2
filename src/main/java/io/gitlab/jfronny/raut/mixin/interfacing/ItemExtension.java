package io.gitlab.jfronny.raut.mixin.interfacing;

public interface ItemExtension {
    public void SetCount(int count);
}
