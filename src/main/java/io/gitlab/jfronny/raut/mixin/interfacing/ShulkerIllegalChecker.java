package io.gitlab.jfronny.raut.mixin.interfacing;

import net.minecraft.item.ItemStack;

import static io.gitlab.jfronny.raut.modules.MiscModule.SHULKER_ILLEGAL;

public class ShulkerIllegalChecker {
    //TODO test
    public static boolean isAllowed(ItemStack stack) {
        //Tag
        return !SHULKER_ILLEGAL.contains(stack.getItem());
        //Should be equal to
        //!(stack.getItem() instanceof BackpackTrinket) && !(Block.getBlockFromItem(stack.getItem()) instanceof ShulkerBoxBlock);
    }
}
