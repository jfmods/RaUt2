package io.gitlab.jfronny.raut.mixin.mixins;

import io.gitlab.jfronny.raut.mixin.interfacing.ItemExtension;
import net.minecraft.item.Item;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Item.class)
public abstract class ItemMixin implements ItemExtension {
    @Mutable
    @Final
    @Shadow
    private int maxCount;

    public void SetCount(int count) {
        maxCount = count;
    }
}
