package io.gitlab.jfronny.raut.mixin;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.mixin.mixins.*;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import static io.gitlab.jfronny.raut.RaUt.cfg;

public class MixinPlugin implements IMixinConfigPlugin {
    @Override
    public void onLoad(String mixinPackage) {

    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        RaUt.loadCfg();
        if (Objects.equals(mixinClassName, ItemMixin.class.getName()))
            return cfg.misc.oreProcessing;
        else if (Objects.equals(mixinClassName, EntityMixin.class.getName()))
            return cfg.aquilorite.aquiloriteArmor.overpowered;
        else if (Objects.equals(mixinClassName, ItemGroupMixin.class.getName()))
            return (cfg.misc.extraCreativeItems) || (cfg.trinkets.enabled && cfg.trinkets.backpack.enabled);
        else if (Objects.equals(mixinClassName, PlayerInventoryMixin.class.getName()))
            return cfg.trinkets.enabled && cfg.trinkets.backpack.enabled;
        else if (Objects.equals(mixinClassName, ShulkerBoxBlockEntityMixin.class.getName()))
            return cfg.shulkerIllegal;
        else if (Objects.equals(mixinClassName, ShulkerBoxSlotMixin.class.getName()))
            return cfg.shulkerIllegal;
        else if (Objects.equals(mixinClassName, RecipeManagerMixin.class.getName()))
            return true;
        else
            throw new IllegalStateException("Unrecognized mixin! This should never happen");
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }
}
