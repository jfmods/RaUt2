package io.gitlab.jfronny.raut.gui;

import dev.emi.trinkets.api.SlotReference;
import dev.emi.trinkets.api.TrinketComponent;
import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.item.trinket.BackpackTrinket;
import io.gitlab.jfronny.raut.modules.TrinketsModule;
import dev.emi.trinkets.api.TrinketsApi;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPlayerInvPanel;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Pair;

import java.util.Optional;
import java.util.function.Consumer;

public class BackpackGuiDescription extends SyncedGuiDescription {
    ItemStack theStack;

    public BackpackGuiDescription(int syncId, PlayerInventory playerInventory, ItemStack stack, BackpackTrinket.Size size, int slotNumber) {
        super(TrinketsModule.BACKPACK_SCREEN_HANDLER, syncId, playerInventory);
        theStack = stack;
        this.playerInventory = playerInventory;
        Consumer<ItemStack> saver;
        if (slotNumber == -1) {
            Optional<TrinketComponent> tc = TrinketsApi.getTrinketComponent(playerInventory.player);
            if (tc.isEmpty()) {
                RaUt.warn("No trinket component identified when one was needed!");
                return;
            }

            TrinketComponent t = tc.get();

            t.getEquipped(stack.getItem());

            saver = s -> {
                //TODO test, maybe remove if not needed
                for (Pair<SlotReference, ItemStack> sl : t.getEquipped(s1 -> s1.equals(stack)))
                    sl.getLeft().inventory().setStack(sl.getLeft().index(), stack);
                t.update();
            };
        } else {
            saver = s -> {
                playerInventory.setStack(slotNumber, s);
                playerInventory.markDirty();
            };
        }
        this.blockInventory = new BackpackInventory(theStack, size, saver);
        WGridPanel rootPanel = (WGridPanel) getRootPanel();
        Text label = stack.hasCustomName() ? stack.getName() : new TranslatableText("gui." + RaUt.MOD_ID + ".backpack");
        rootPanel.add(new WLabel(label, WLabel.DEFAULT_TEXT_COLOR), 0, 0);
        int rows = (int) Math.ceil(blockInventory.size() / 9.0);
        for (int i = 1; i <= rows; i++) {
            for (int j = 0; j < 9; j++) {
                WItemSlot slot = WItemSlot.of(blockInventory, ((i - 1) * 9) + j);
                rootPanel.add(slot, j, i);
            }
        }
        WPlayerInvPanel playerInv = this.createPlayerInventoryPanel();
        rootPanel.add(playerInv, 0, rows + 2);
        rootPanel.validate(this);
    }
}
