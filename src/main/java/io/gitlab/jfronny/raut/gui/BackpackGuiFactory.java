package io.gitlab.jfronny.raut.gui;

import io.gitlab.jfronny.raut.item.trinket.BackpackTrinket;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;

public class BackpackGuiFactory implements ExtendedScreenHandlerFactory {
    BackpackTrinket.Size size;
    ItemStack stack;

    public BackpackGuiFactory(ItemStack stack, BackpackTrinket.Size size) {
        super();
        this.size = size;
        this.stack = stack;
    }

    @Override
    public Text getDisplayName() {
        return null;
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new BackpackGuiDescription(syncId, inv, stack, size, player.getInventory().getSlotWithStack(stack));
    }

    @Override
    public void writeScreenOpeningData(ServerPlayerEntity serverPlayerEntity, PacketByteBuf packetByteBuf) {
        packetByteBuf.writeItemStack(stack);
        packetByteBuf.writeInt(serverPlayerEntity.getInventory().getSlotWithStack(stack));
    }
}
