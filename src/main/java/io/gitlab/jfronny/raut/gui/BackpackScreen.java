package io.gitlab.jfronny.raut.gui;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

public class BackpackScreen extends CottonInventoryScreen<BackpackGuiDescription> {
    public BackpackScreen(BackpackGuiDescription gui, PlayerEntity player) {
        super(gui, player);
    }
}
