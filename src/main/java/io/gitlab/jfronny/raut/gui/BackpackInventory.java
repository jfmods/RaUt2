package io.gitlab.jfronny.raut.gui;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.item.trinket.BackpackTrinket;
import io.gitlab.jfronny.raut.mixin.interfacing.ShulkerIllegalChecker;
import io.gitlab.jfronny.raut.util.ImplementedInventory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;

import java.util.function.Consumer;

public class BackpackInventory implements ImplementedInventory {
    private final ItemStack backpackStack;
    private DefaultedList<ItemStack> contentList;
    private final Consumer<ItemStack> saver;

    public BackpackInventory(ItemStack stack, BackpackTrinket.Size size, Consumer<ItemStack> saver) {
        backpackStack = stack;
        contentList = DefaultedList.ofSize((switch (size) {
            case Medium -> RaUt.cfg.trinkets.backpack.mediumRows;
            case Large -> RaUt.cfg.trinkets.backpack.largeRows;
            default -> RaUt.cfg.trinkets.backpack.smallRows;
        }) * 9, ItemStack.EMPTY);
        this.saver = saver;
        if (stack.getTag() != null && stack.getTag().contains("Inventory"))
            Inventories.readNbt(backpackStack.getTag().getCompound("Inventory"), contentList);
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return contentList;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, Direction side) {
        return ShulkerIllegalChecker.isAllowed(stack);
    }

    @Override
    public void markDirty() {
        NbtCompound invTag = null;
        if (backpackStack.getTag() != null) {
            invTag = backpackStack.getTag().getCompound("Inventory");
        }
        if (contentList == null) {
            contentList = DefaultedList.ofSize(9, ItemStack.EMPTY);
        }
        invTag = Inventories.writeNbt(invTag, contentList);
        backpackStack.getTag().put("Inventory", invTag);
        saver.accept(backpackStack);
    }

    @Override
    public void onClose(PlayerEntity playerEntity_1) {
        markDirty();
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        return canInsert(slot, stack, Direction.DOWN);
    }
}
