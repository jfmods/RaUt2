package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import io.gitlab.jfronny.raut.util.data.TagBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;


public class DataFixerModule extends BaseModule {
    @Override
    public void init() {
        for (Item item : DepRegistry.disabledItems.values()) {
            RaUt.debug("Block: " + item.toString());
            ItemStack stack = new ItemStack(item);
            RecipeUtil.removeRecipeFor(stack);
        }
        for (TagBuilder builder : TagBuilder.TAG_BUILDERS) {
            builder.apply();
        }
    }
}
