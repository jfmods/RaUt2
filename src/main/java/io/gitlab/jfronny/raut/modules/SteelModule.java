package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.util.AttributeArmorMat;
import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.util.MiningLevel;
import io.gitlab.jfronny.raut.item.armor.SteelArmorMat;
import io.gitlab.jfronny.raut.item.tools.SteelToolMat;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.*;

public class SteelModule extends BaseModule {
    public static final Item RAW_STEEL = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item STEEL_INGOT = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item STEEL_NUGGET = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Block STEEL_BLOCK = new Block(FabricBlockSettings.of(Material.METAL).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.IRON).strength(5, 6));
    public static final AttributeArmorMat STEEL_ARMOR = new SteelArmorMat();

    @Override
    public void init() {
        DepRegistry.registerTools("steel", cfg.steel.tools, new SteelToolMat());
        boolean cottonRes = FabricLoader.getInstance().isModLoaded("cotton-resources");
        DepRegistry.registerItem("raw_steel", cfg.steel.enabled || ((cfg.aquilorite.enabled && cfg.aquilorite.aquiloriteBlockHardened) && !cottonRes), RAW_STEEL);
        DepRegistry.registerItem("steel_ingot", cfg.steel.enabled || ((cfg.aquilorite.enabled && cfg.aquilorite.aquiloriteBlockHardened) && !cottonRes), STEEL_INGOT);
        DepRegistry.registerItem(STEEL_NUGGETS.add("steel_nugget", cfg.steel.nugget), cfg.steel.nugget, STEEL_NUGGET);
        DepRegistry.registerBlock(STEEL_BLOCKS.add("steel_block", cfg.steel.block), cfg.steel.block, STEEL_BLOCK);
        DepRegistry.registerArmor("steel", cfg.steel.armor, STEEL_ARMOR, NONE);
    }
}
