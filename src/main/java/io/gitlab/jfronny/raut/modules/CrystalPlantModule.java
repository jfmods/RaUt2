package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.util.GenericPlant;
import io.gitlab.jfronny.raut.item.Crystal;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplierBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.*;

public class CrystalPlantModule extends BaseModule {
    public static final GenericPlant CRYSTAL_PLANT = new GenericPlant();
    public static final BlockItem CRYSTAL_PLANT_SEED = CRYSTAL_PLANT.seed;
    public static final Item CRYSTAL = new Crystal();

    @Override
    public void init() {
        DepRegistry.registerBlock(PLANTABLES.add("crystal_plant", cfg.crystalPlant.enabled), cfg.crystalPlant.enabled, CRYSTAL_PLANT, CRYSTAL_PLANT_SEED);
        DepRegistry.registerItem("crystal", cfg.crystalPlant.enabled, CRYSTAL);
        if (!cfg.crystalPlant.enabled || !cfg.crystalPlant.craftGApples) {
            RecipeUtil.removeRecipe("raut:crystal_apple");
        }
        if (!cfg.crystalPlant.enabled || !cfg.crystalPlant.craftGApples2) {
            RecipeUtil.removeRecipe("raut:crystal_enchanted_apple");
        }
    }

    @Override
    public void onLootTableLoading(ResourceManager resourceManager, LootManager lootManager, Identifier id, FabricLootSupplierBuilder supplier, LootTableLoadingCallback.LootTableSetter setter) {
        if ((id.equals(new Identifier("chests/abandoned_mineshaft")) || id.equals(new Identifier("chests/desert_pyramid")) || id.equals(new Identifier("chests/jungle_temple")))) {
            if (cfg.crystalPlant.enabled) {
                FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder
                        .builder()
                        .rolls(ConstantLootNumberProvider.create(1))
                        .withCondition(RandomChanceLootCondition.builder(cfg.crystalPlant.chestChance).build())
                        .withEntry(ItemEntry.builder(CRYSTAL).build())
                        .withEntry(ItemEntry.builder(CRYSTAL_PLANT_SEED).build());
                supplier.withPool(poolBuilder.build());
            }
        }
    }

    @Override
    public void initClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(CRYSTAL_PLANT, RenderLayer.getCutout());
    }
}
