package io.gitlab.jfronny.raut.modules;

import com.google.common.collect.ImmutableMap;
import dev.emi.trinkets.api.SlotReference;
import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.gui.BackpackGuiDescription;
import io.gitlab.jfronny.raut.item.trinket.AngelRing;
import io.gitlab.jfronny.raut.item.trinket.BackpackTrinket;
import io.gitlab.jfronny.raut.item.trinket.BuilderRing;
import io.gitlab.jfronny.raut.item.trinket.TravellersRing;
import dev.emi.trinkets.api.TrinketsApi;
import io.gitlab.jfronny.raut.util.data.TagBuilder;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;

import java.util.Map;

import static io.gitlab.jfronny.raut.RaUt.cfg;

public class TrinketsModule extends BaseModule {
    public static final Identifier OPEN_BACKPACK_PACKET_ID = new Identifier(RaUt.MOD_ID, "open_backpack");
    public static final Map<BackpackTrinket.Size, BackpackTrinket> backpacks = ImmutableMap.of(
            BackpackTrinket.Size.Small, new BackpackTrinket(BackpackTrinket.Size.Small),
            BackpackTrinket.Size.Medium, new BackpackTrinket(BackpackTrinket.Size.Medium),
            BackpackTrinket.Size.Large, new BackpackTrinket(BackpackTrinket.Size.Large)
    );
    public static ScreenHandlerType<BackpackGuiDescription> BACKPACK_SCREEN_HANDLER;

    @Override
    public void init() {
        DepRegistry.registerItem("traveller_ring", cfg.trinkets.enabled && cfg.trinkets.traveller_ring.enabled, new TravellersRing());
        DepRegistry.registerItem("angel_ring", cfg.trinkets.enabled && cfg.trinkets.angel_ring, new AngelRing());
        DepRegistry.registerItem("builders_ring", cfg.trinkets.enabled && cfg.trinkets.builders_ring.enabled, new BuilderRing());
        boolean backpack = cfg.trinkets.enabled && cfg.trinkets.backpack.enabled;
        for (BackpackTrinket e : backpacks.values()) {
            DepRegistry.registerItem(TagBuilder.SHULKER_BOXES_ILLEGAL.add(e.getSize().name().toLowerCase() + "_backpack", backpack), backpack, e);
        }
        if (cfg.trinkets.enabled && cfg.trinkets.backpack.enabled) {

            BACKPACK_SCREEN_HANDLER = ScreenHandlerRegistry.registerExtended(new Identifier(RaUt.MOD_ID, "backpack"), (syncId, player, buf) -> {
                ItemStack stack = buf.readItemStack();
                int slotNumber = buf.readInt();
                if (stack.getItem() instanceof BackpackTrinket bi) {
                    return new BackpackGuiDescription(syncId, player.player.getInventory(), stack, bi.getSize(), slotNumber);
                }
                else {
                    System.err.println("Could not open the backpack UI because the item is no backpack");
                    return null;
                }
            });
            ServerPlayNetworking.registerGlobalReceiver(OPEN_BACKPACK_PACKET_ID, (server, player, handler, buf, responseSender) -> {
                server.execute(() -> {
                    var tc = TrinketsApi.getTrinketComponent(player);
                    if (tc.isEmpty()) return;
                    for (BackpackTrinket e : backpacks.values()) {
                        for (Pair<SlotReference, ItemStack> sr : tc.get().getEquipped(e)) {
                            e.openGUI(sr.getRight(), player);
                            break;
                        }
                    }
                });
            });
        }
    }
}
