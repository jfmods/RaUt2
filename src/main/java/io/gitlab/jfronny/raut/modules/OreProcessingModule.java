package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.mixin.interfacing.ItemExtension;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Items;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.GOLD_DUSTS;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.IRON_DUSTS;

public class OreProcessingModule extends BaseModule {
    public static final Item IRON_SHARD = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item IRON_DUST = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item GOLD_SHARD = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item GOLD_DUST = new Item(new Item.Settings().group(ItemGroup.MATERIALS));

    @Override
    public void init() {
        DepRegistry.registerItem("iron_shard", cfg.misc.oreProcessing, IRON_SHARD);
        DepRegistry.registerItem(IRON_DUSTS.add("iron_dust", cfg.misc.oreProcessing), cfg.misc.oreProcessing, IRON_DUST);
        DepRegistry.registerItem("gold_shard", cfg.misc.oreProcessing, GOLD_SHARD);
        DepRegistry.registerItem(GOLD_DUSTS.add("gold_dust", cfg.misc.oreProcessing), cfg.misc.oreProcessing, GOLD_DUST);
        if (cfg.misc.oreProcessing) {
            ((ItemExtension) Items.ANVIL).SetCount(1);
        } else {
            RecipeUtil.removeRecipe("raut:iron_dust");
            RecipeUtil.removeRecipe("raut:iron_shard");
            RecipeUtil.removeRecipe("raut:iron_ingot");
            RecipeUtil.removeRecipe("raut:iron_ingot2");
            RecipeUtil.removeRecipe("raut:gold_dust");
            RecipeUtil.removeRecipe("raut:gold_shard");
            RecipeUtil.removeRecipe("raut:gold_ingot");
            RecipeUtil.removeRecipe("raut:gold_ingot2");
            RecipeUtil.removeRecipe("raut:aquilorite_proc");
            RecipeUtil.removeRecipe("raut:coal_proc");
            RecipeUtil.removeRecipe("raut:diamond_proc");
            RecipeUtil.removeRecipe("raut:emerald_proc");
            RecipeUtil.removeRecipe("raut:lapis_proc");
            RecipeUtil.removeRecipe("raut:nether_quartz_proc");
        }
    }
}
