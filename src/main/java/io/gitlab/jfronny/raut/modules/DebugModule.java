package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.item.DebugClear;
import net.minecraft.item.Item;

import static io.gitlab.jfronny.raut.RaUt.cfg;

public class DebugModule extends BaseModule {
    public static final Item DEBUG_CLEAR = new DebugClear();

    @Override
    public void init() {
        DepRegistry.registerItem("debug_clear", cfg.debug.enabled && cfg.debug.debugClear.enabled, DEBUG_CLEAR);
    }
}
