package io.gitlab.jfronny.raut.modules;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.util.ToolMatBuilder;
import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.util.GenericPlant;
import io.gitlab.jfronny.raut.util.MiningLevel;
import io.gitlab.jfronny.raut.block.*;
import io.gitlab.jfronny.raut.item.StaffOfTime;
import io.gitlab.jfronny.raut.mixin.interfacing.ItemGroupExtension;
import io.gitlab.jfronny.raut.util.basicItem.BasePaxel;
import io.gitlab.jfronny.raut.util.basicItem.PaxelItem;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import io.gitlab.jfronny.raut.util.data.TagBuilder;
import net.devtech.arrp.json.loot.JCondition;
import net.devtech.arrp.json.loot.JFunction;
import net.devtech.arrp.json.loot.JLootTable;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplierBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.tag.TagRegistry;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.*;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.resource.ResourceManager;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.PAXELS;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.PLANTABLES;

public class MiscModule extends BaseModule {
    public static final Block BOOST = new Block(FabricBlockSettings.of(Material.GLASS).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.IRON).slipperiness(cfg.misc.boostBlock.slipperiness).strength(cfg.misc.boostBlock.hardness, cfg.misc.boostBlock.resistance));
    public static final Item CHAIN_PLATE = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Block ANGEL_BLOCK = new AngelBlock();
    public static final Item GLASS_SHARD = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final GenericPlant END_PLANT = new GenericPlant();
    public static final BlockItem END_PLANT_SEED = END_PLANT.seed;
    public static final Block COBBLE_GENERATOR = new CobbleGeneratorBlock();
    public static final BlockItem COBBLE_GENERATOR_ITEM = new BlockItem(COBBLE_GENERATOR, new Item.Settings().group(ItemGroup.DECORATIONS));
    public static final BlockEntityType<CobbleGeneratorEntity> COBBLE_GENERATOR_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(RaUt.MOD_ID, "cobble_generator"), FabricBlockEntityTypeBuilder.create(CobbleGeneratorEntity::new, COBBLE_GENERATOR).build(null));
    public static final Block BLOCK_BREAKER = new BlockBreaker();
    public static Tag<Item> SHULKER_ILLEGAL = TagRegistry.item(new Identifier(RaUt.MOD_ID, "shulker_boxes_illegal"));
    public static Tag<Item> CHAIN_PLATE_TAG = TagRegistry.item(new Identifier(RaUt.MOD_ID, "chain_plate"));

    private static final int damageAdded = 3;
    private static final int goldDamage = 4;

    public static final PaxelItem WOODEN_PAXEL = new BasePaxel(ToolMaterials.WOOD, (int) ToolMaterials.WOOD.getAttackDamage() + damageAdded, (AxeItem) Items.WOODEN_AXE, (SwordItem) Items.WOODEN_SWORD, (PickaxeItem) Items.WOODEN_PICKAXE, (ShovelItem) Items.WOODEN_SHOVEL, (HoeItem) Items.WOODEN_HOE);
    public static final PaxelItem STONE_PAXEL = new BasePaxel(ToolMaterials.STONE, (int) ToolMaterials.STONE.getAttackDamage() + damageAdded, (AxeItem) Items.STONE_AXE, (SwordItem) Items.STONE_SWORD, (PickaxeItem) Items.STONE_PICKAXE, (ShovelItem) Items.STONE_SHOVEL, (HoeItem) Items.STONE_HOE);
    public static final PaxelItem IRON_PAXEL = new BasePaxel(ToolMaterials.IRON, (int) ToolMaterials.IRON.getAttackDamage() + damageAdded, (AxeItem) Items.IRON_AXE, (SwordItem) Items.IRON_SWORD, (PickaxeItem) Items.IRON_PICKAXE, (ShovelItem) Items.IRON_SHOVEL, (HoeItem) Items.IRON_HOE);
    public static final PaxelItem GOLDEN_PAXEL = new BasePaxel(new ToolMatBuilder(ToolMaterials.GOLD).setAttackDamage(goldDamage), goldDamage, (AxeItem) Items.GOLDEN_AXE, (SwordItem) Items.GOLDEN_SWORD, (PickaxeItem) Items.GOLDEN_PICKAXE, (ShovelItem) Items.GOLDEN_SHOVEL, (HoeItem) Items.GOLDEN_HOE);
    public static final PaxelItem DIAMOND_PAXEL = new BasePaxel(ToolMaterials.DIAMOND, (int) ToolMaterials.DIAMOND.getAttackDamage() + damageAdded, (AxeItem) Items.DIAMOND_AXE, (SwordItem) Items.DIAMOND_SWORD, (PickaxeItem) Items.DIAMOND_PICKAXE, (ShovelItem) Items.DIAMOND_SHOVEL, (HoeItem) Items.DIAMOND_HOE);

    public static final Item STAFF_OF_TIME = new StaffOfTime();
    public static final Tag<Block> STAFF_OF_TIME_ILLEGAL = TagRegistry.block(new Identifier(RaUt.MOD_ID, "staff_of_time_illegal"));

    @Override
    public void init() {
        DepRegistry.registerBlock("boost", cfg.misc.boostBlock.enabled, BOOST);
        DepRegistry.registerItem("chain_plate", cfg.misc.chainPlate, CHAIN_PLATE);

        boolean hasPlate = false;
        if (cfg.misc.chainPlate) {
            TagBuilder.CHAIN_PLATE.add("chain_plate", true);
            hasPlate = true;
        }
        if (FabricLoader.getInstance().isModLoaded("byg")) {
            TagBuilder.CHAIN_PLATE.add(new Identifier("byg", "chain_plating"), true);
            hasPlate = true;
        }
        if (!hasPlate) {
            TagBuilder.CHAIN_PLATE.add(new Identifier("minecraft", "iron_ingot"), true);
        }

        DepRegistry.registerBlock("angelblock", cfg.misc.angelBlock.enabled, ANGEL_BLOCK, new AngelBlockItem());
        DepRegistry.registerItem("glass_shard", cfg.misc.glassShards, GLASS_SHARD);
        DepRegistry.registerBlock(PLANTABLES.add("end_plant", cfg.misc.endPlant.enabled), cfg.misc.endPlant.enabled, END_PLANT, END_PLANT_SEED);
        DepRegistry.registerBlock("cobble_generator", cfg.misc.cobbleGen.enabled, COBBLE_GENERATOR, COBBLE_GENERATOR_ITEM);
        DepRegistry.registerBlock("block_breaker", cfg.misc.blockBreaker.enabled, BLOCK_BREAKER, ItemGroup.REDSTONE);
        DepRegistry.registerItem("staff_of_time", cfg.misc.staffOfTime.enabled, STAFF_OF_TIME);

        DepRegistry.registerItem(PAXELS.add("wooden_paxel", cfg.misc.vanillaPaxels), cfg.misc.vanillaPaxels, WOODEN_PAXEL);
        DepRegistry.registerItem(PAXELS.add("stone_paxel", cfg.misc.vanillaPaxels), cfg.misc.vanillaPaxels, STONE_PAXEL);
        DepRegistry.registerItem(PAXELS.add("iron_paxel", cfg.misc.vanillaPaxels), cfg.misc.vanillaPaxels, IRON_PAXEL);
        DepRegistry.registerItem(PAXELS.add("golden_paxel", cfg.misc.vanillaPaxels), cfg.misc.vanillaPaxels, GOLDEN_PAXEL);
        DepRegistry.registerItem(PAXELS.add("diamond_paxel", cfg.misc.vanillaPaxels), cfg.misc.vanillaPaxels, DIAMOND_PAXEL);

        if (cfg.misc.betterDiamondRecipe) {
            RecipeUtil.removeRecipe("minecraft:leather_horse_armor");
            RecipeUtil.removeRecipe("minecraft:diamond_boots");
            RecipeUtil.removeRecipe("minecraft:diamond_chestplate");
            RecipeUtil.removeRecipe("minecraft:diamond_helmet");
            RecipeUtil.removeRecipe("minecraft:diamond_leggings");
        } else {
            RecipeUtil.removeRecipe("raut:diamond_boots");
            RecipeUtil.removeRecipe("raut:diamond_chestplate");
            RecipeUtil.removeRecipe("raut:diamond_helmet");
            RecipeUtil.removeRecipe("raut:diamond_leggings");
        }
        if (!cfg.misc.chainmailRecipe) {
            RecipeUtil.removeRecipe("raut:chainmail_boots");
            RecipeUtil.removeRecipe("raut:chainmail_chestplate");
            RecipeUtil.removeRecipe("raut:chainmail_helmet");
            RecipeUtil.removeRecipe("raut:chainmail_leggings");
        }
        if (!cfg.misc.horseArmorRecipe) {
            RecipeUtil.removeRecipe("raut:leather_horse_armor");
            RecipeUtil.removeRecipe("raut:diamond_horse_armor");
            RecipeUtil.removeRecipe("raut:golden_horse_armor");
            RecipeUtil.removeRecipe("raut:iron_horse_armor");
        }
        if (!cfg.misc.glassShards) {
            RecipeUtil.removeRecipe("raut:glass_shard");
        }
        else {
            Gson g = new Gson();
            RaUt.DATA_PACK.addLootTable(new Identifier("minecraft", "blocks/glass"), JLootTable.loot("minecraft:block").pool(
                    JLootTable.pool().rolls(1).entry(JLootTable.entry().type("minecraft:alternatives").child(
                            JLootTable.entry().type("minecraft:item").name("minecraft:glass").condition(
                                    JLootTable.predicate("minecraft:match_tool").parameter("predicate", g.fromJson("""
{
  "enchantments": [
    {
      "enchantment": "minecraft:silk_touch",
      "levels": {
        "min": 1
      }
    }
  ]
}""", JsonObject.class)) //TODO test
                            )
                    ).child(
                            JLootTable.entry().type("minecraft:item").name("raut:glass_shard").function(JLootTable.function("minecraft:set_count")
                                    .parameter("count", g.fromJson("""
{
  "min": 2.0,
  "max": 3.0,
  "type": "minecraft:uniform"
}""", JsonObject.class)))
                                .function("minecraft:explosion_decay")
                    ))
            ));
        }
    }

    @Override
    public void onLootTableLoading(ResourceManager resourceManager, LootManager lootManager, Identifier id, FabricLootSupplierBuilder supplier, LootTableLoadingCallback.LootTableSetter setter) {
        if ((id.equals(new Identifier("blocks/grass")) || id.equals(new Identifier("blocks/fern")) || id.equals(new Identifier("blocks/tall_grass")))) {
            if (cfg.misc.moreSeeds.enabled) {
                FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder
                        .builder()
                        .rolls(ConstantLootNumberProvider.create(1))
                        .withCondition(RandomChanceLootCondition.builder(cfg.misc.moreSeeds.dropChance).build())
                        .withEntry(ItemEntry.builder(Items.BEETROOT_SEEDS).build())
                        .withEntry(ItemEntry.builder(Items.MELON_SEEDS).build())
                        .withEntry(ItemEntry.builder(Items.PUMPKIN_SEEDS).build());
                supplier.withPool(poolBuilder.build());
            }
        }
        if ((id.equals(new Identifier("chests/abandoned_mineshaft")) || id.equals(new Identifier("chests/desert_pyramid")) || id.equals(new Identifier("chests/jungle_temple")))) {
            if (cfg.misc.endPlant.enabled) {
                FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder
                        .builder()
                        .rolls(ConstantLootNumberProvider.create(1))
                        .withCondition(RandomChanceLootCondition.builder(cfg.misc.endPlant.chestChance).build())
                        .withEntry(ItemEntry.builder(END_PLANT_SEED).build());
                supplier.withPool(poolBuilder.build());
            }
        }
    }

    @Override
    public void initClient() {
        if (cfg.misc.extraCreativeItems) {
            ItemGroupExtension SEARCH = (ItemGroupExtension) ItemGroup.SEARCH;

            ItemGroupExtension REDSTONE = (ItemGroupExtension) ItemGroup.REDSTONE;
            REDSTONE.addStack(new ItemStack(Items.COMMAND_BLOCK));
            SEARCH.addStack(new ItemStack(Items.COMMAND_BLOCK));
            REDSTONE.addStack(new ItemStack(Items.CHAIN_COMMAND_BLOCK));
            SEARCH.addStack(new ItemStack(Items.CHAIN_COMMAND_BLOCK));
            REDSTONE.addStack(new ItemStack(Items.REPEATING_COMMAND_BLOCK));
            SEARCH.addStack(new ItemStack(Items.REPEATING_COMMAND_BLOCK));
            REDSTONE.addStack(new ItemStack(Items.STRUCTURE_BLOCK));
            SEARCH.addStack(new ItemStack(Items.STRUCTURE_BLOCK));
            REDSTONE.addStack(new ItemStack(Items.JIGSAW));
            SEARCH.addStack(new ItemStack(Items.JIGSAW));

            ItemGroupExtension TRANSPORT = (ItemGroupExtension) ItemGroup.TRANSPORTATION;
            TRANSPORT.addStack(new ItemStack(Items.COMMAND_BLOCK_MINECART));
            SEARCH.addStack(new ItemStack(Items.COMMAND_BLOCK_MINECART));

            ItemGroupExtension DECORATIONS = (ItemGroupExtension) ItemGroup.DECORATIONS;
            DECORATIONS.addStack(new ItemStack(Items.DRAGON_EGG));
            SEARCH.addStack(new ItemStack(Items.DRAGON_EGG));
            DECORATIONS.addStack(new ItemStack(Items.BARRIER));
            SEARCH.addStack(new ItemStack(Items.BARRIER));
            DECORATIONS.addStack(new ItemStack(Items.SPAWNER));
            SEARCH.addStack(new ItemStack(Items.SPAWNER));
            DECORATIONS.addStack(new ItemStack(Items.STRUCTURE_VOID));
            SEARCH.addStack(new ItemStack(Items.STRUCTURE_VOID));

            ItemGroupExtension MISC = (ItemGroupExtension) ItemGroup.MISC;
            MISC.addStack(new ItemStack(Items.DEBUG_STICK));
            SEARCH.addStack(new ItemStack(Items.DEBUG_STICK));
            MISC.addStack(new ItemStack(Items.FILLED_MAP));
            SEARCH.addStack(new ItemStack(Items.FILLED_MAP));
            MISC.addStack(new ItemStack(Items.WRITTEN_BOOK));
            SEARCH.addStack(new ItemStack(Items.WRITTEN_BOOK));
            MISC.addStack(new ItemStack(Items.KNOWLEDGE_BOOK));
            SEARCH.addStack(new ItemStack(Items.KNOWLEDGE_BOOK));
        }
        if (cfg.misc.endPlant.enabled) {
            BlockRenderLayerMap.INSTANCE.putBlock(END_PLANT, RenderLayer.getCutout());
        }
    }
}
