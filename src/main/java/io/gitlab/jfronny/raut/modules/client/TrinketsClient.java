package io.gitlab.jfronny.raut.modules.client;

import dev.emi.trinkets.api.SlotReference;
import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.item.trinket.BackpackTrinket;
import io.gitlab.jfronny.raut.util.ClientModule;
import io.gitlab.jfronny.raut.gui.BackpackGuiDescription;
import io.gitlab.jfronny.raut.gui.BackpackScreen;
import io.gitlab.jfronny.raut.mixin.interfacing.ItemGroupExtension;
import io.gitlab.jfronny.raut.modules.TrinketsModule;
import dev.emi.trinkets.api.TrinketsApi;
import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Pair;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.modules.TrinketsModule.OPEN_BACKPACK_PACKET_ID;
import static io.gitlab.jfronny.raut.modules.TrinketsModule.backpacks;

public class TrinketsClient extends ClientModule {
    @Override
    @Environment(EnvType.CLIENT)
    public void initClient() {
        if (cfg.trinkets.enabled && cfg.trinkets.backpack.enabled) {
            ScreenRegistry.<BackpackGuiDescription, CottonInventoryScreen<BackpackGuiDescription>>register(TrinketsModule.BACKPACK_SCREEN_HANDLER,
                    (gui, inventory, title) -> new BackpackScreen(gui, inventory.player));
            KeyBinding backpackKeyBinding = new KeyBinding(
                    "key." + RaUt.MOD_ID + ".open_backpack",
                    InputUtil.Type.KEYSYM,
                    cfg.trinkets.backpack.defaultKey,
                    "key.categories.inventory"
            );
            ClientTickEvents.END_CLIENT_TICK.register(client -> {
                if (backpackKeyBinding.isPressed()) {
                    var tc = TrinketsApi.getTrinketComponent(client.player);
                    if (tc.isEmpty()) return;
                    for (BackpackTrinket backpack : backpacks.values()) {
                        for (Pair<SlotReference, ItemStack> sl : tc.get().getEquipped(backpack)) {
                            PacketByteBuf passedData = new PacketByteBuf(Unpooled.buffer());
                            passedData.writeItemStack(sl.getRight());
                            ClientPlayNetworking.send(OPEN_BACKPACK_PACKET_ID, passedData);
                            return;
                        }
                    }
                }
            });
            KeyBindingHelper.registerKeyBinding(backpackKeyBinding);
        }
    }
}
