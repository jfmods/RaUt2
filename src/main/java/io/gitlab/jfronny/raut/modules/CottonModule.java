package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.DepRegistry;
import io.gitlab.jfronny.raut.util.GenericPlant;
import io.gitlab.jfronny.raut.util.data.RecipeUtil;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.loot.v1.FabricLootPoolBuilder;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplierBuilder;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.BlockItem;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.condition.RandomChanceLootCondition;
import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.provider.number.ConstantLootNumberProvider;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;

import static io.gitlab.jfronny.raut.RaUt.cfg;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.PLANTABLES;

public class CottonModule extends BaseModule {
    public static final GenericPlant COTTON_CROP = new GenericPlant();
    public static final BlockItem COTTON_SEED = COTTON_CROP.seed;

    @Override
    public void init() {
        DepRegistry.registerBlock(PLANTABLES.add("cotton", cfg.cotton.enabled), cfg.cotton.enabled, COTTON_CROP, COTTON_SEED);
        if (!cfg.cotton.enabled) {
            RecipeUtil.removeRecipe("raut:cotton_string");
        }
    }

    @Override
    public void onLootTableLoading(ResourceManager resourceManager, LootManager lootManager, Identifier id, FabricLootSupplierBuilder supplier, LootTableLoadingCallback.LootTableSetter setter) {
        if ((id.equals(new Identifier("blocks/grass")) || id.equals(new Identifier("blocks/fern")) || id.equals(new Identifier("blocks/tall_grass")))) {
            if (cfg.cotton.enabled) {
                FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder
                        .builder()
                        .rolls(ConstantLootNumberProvider.create(1))
                        .withCondition(RandomChanceLootCondition.builder(cfg.cotton.dropChance).build())
                        .withEntry(ItemEntry.builder(COTTON_SEED).build());
                supplier.withPool(poolBuilder.build());
            }
        }
    }

    @Override
    public void initClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(COTTON_CROP, RenderLayer.getCutout());
    }
}
