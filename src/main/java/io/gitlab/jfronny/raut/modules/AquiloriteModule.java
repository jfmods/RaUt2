package io.gitlab.jfronny.raut.modules;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.item.armor.AquiloriteArmorMat;
import io.gitlab.jfronny.raut.item.AquiloriteGun;
import io.gitlab.jfronny.raut.item.tools.AquiloriteToolMat;
import io.gitlab.jfronny.raut.util.*;
import io.gitlab.jfronny.raut.util.basicItem.BaseModule;
import io.gitlab.jfronny.raut.util.basicItem.BasePaxel;
import io.gitlab.jfronny.raut.util.data.TagBuilder;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tag.TagRegistry;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.*;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.heightprovider.UniformHeightProvider;

import static io.gitlab.jfronny.raut.RaUt.cfg;

public class AquiloriteModule extends BaseModule {
    public static final Block AQUILORITE_ORE = new Block(FabricBlockSettings.of(Material.STONE).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.DIAMOND).strength(2, 2));
    public static final Block AQUILORITE_BLOCK = new Block(FabricBlockSettings.of(Material.GLASS).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.IRON).noCollision().strength(2, 2));
    public static final Block AQUILORITE_BLOCK_2 = new Block(FabricBlockSettings.of(Material.GLASS).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.IRON).slipperiness(1).strength(3, 3));
    public static final Item AQUILORITE_GEM = new Item(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final Item AQUILORITE_MULTITOOL = new BasePaxel(new AquiloriteToolMat(), 7, (AxeItem) Items.DIAMOND_AXE, (SwordItem) Items.DIAMOND_SWORD, (PickaxeItem) Items.DIAMOND_PICKAXE, (ShovelItem) Items.DIAMOND_SHOVEL, (HoeItem) Items.DIAMOND_HOE, () -> cfg.aquilorite.aquiloritePaxel.overpoweredDamage);
    public static final ArrowItem EXPLOSIVE_SHARD = new ArrowItem(new Item.Settings().group(ItemGroup.MATERIALS));
    public static final AquiloriteGun AQUILORITE_GUN = new AquiloriteGun();
    public static final AttributeArmorMat AQUILORITE_ARMOR = new AquiloriteArmorMat();
    public static Tag<Item> AQUILORITE_GEM_TAG = TagRegistry.item(new Identifier(RaUt.MOD_ID, "aquilorite_gem"));
    public static Tag<Item> AQUILORITE_ARMOR_TAG = TagRegistry.item(new Identifier(RaUt.MOD_ID, "aquilorite_armor"));
    public static ConfiguredFeature<?, ?> AQUILORITE_ORE_OVERWORLD = Feature.ORE
            .configure(new OreFeatureConfig(
                    OreFeatureConfig.Rules.BASE_STONE_OVERWORLD,
                    AQUILORITE_ORE.getDefaultState(),
                    3
            )).decorate(Decorator.RANGE.configure(new RangeDecoratorConfig(
                    UniformHeightProvider.create(YOffset.aboveBottom(0), YOffset.aboveBottom(64))
            )))
            .spreadHorizontally()
            .repeat(4);

    @Override
    public void init() {
        DepRegistry.registerArmor("aquilorite", cfg.aquilorite.enabled && cfg.aquilorite.aquiloriteArmor.enabled, AQUILORITE_ARMOR, TagBuilder.AQUILORITE_ARMOR);
        DepRegistry.registerBlock("aquilorite_ore", cfg.aquilorite.enabled, AQUILORITE_ORE);
        DepRegistry.registerBlock("aquilorite_block", cfg.aquilorite.enabled && cfg.aquilorite.aquiloriteBlock, AQUILORITE_BLOCK);
        DepRegistry.registerBlock("aquilorite_block_hardened", cfg.aquilorite.enabled && cfg.aquilorite.aquiloriteBlockHardened, AQUILORITE_BLOCK_2);
        DepRegistry.registerItem("aquilorite_gem", cfg.aquilorite.enabled, AQUILORITE_GEM);
        boolean paxel = cfg.aquilorite.enabled && cfg.aquilorite.aquiloritePaxel.enabled;
        DepRegistry.registerItem(TagBuilder.PAXELS.add("aquilorite_paxel", paxel), paxel, AQUILORITE_MULTITOOL);
        DepRegistry.registerItem("explosive_shard", cfg.aquilorite.enabled && cfg.aquilorite.gun.enabled, EXPLOSIVE_SHARD);
        DepRegistry.registerItem("aquilorite_gun", cfg.aquilorite.enabled && cfg.aquilorite.gun.enabled, AQUILORITE_GUN);

        RegistryKey<ConfiguredFeature<?, ?>> aquiloriteOreOverworld = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY,
                new Identifier(RaUt.MOD_ID, "aquilorite_ore"));
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, aquiloriteOreOverworld.getValue(), AQUILORITE_ORE_OVERWORLD);
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, aquiloriteOreOverworld);

        if (cfg.aquilorite.enabled) {
            TagBuilder.AQUILORITE_GEM.add("aquilorite_gem", true);
        }
        else {
            TagBuilder.AQUILORITE_GEM.add(new Identifier("minecraft", "diamond"), true);
        }
    }
}
