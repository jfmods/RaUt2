package io.gitlab.jfronny.raut;

import io.gitlab.jfronny.raut.util.ClientModule;
import io.gitlab.jfronny.raut.modules.client.TrinketsClient;
import net.fabricmc.api.ClientModInitializer;

import java.util.ArrayList;

public class RaUtClient implements ClientModInitializer {
    public static final ArrayList<ClientModule> modules = new ArrayList<>();

    @Override
    public void onInitializeClient() {
        modules.addAll(RaUt.modules);
        modules.add(new TrinketsClient());
        for (ClientModule module : modules) {
            module.initClient();
        }
    }
}
