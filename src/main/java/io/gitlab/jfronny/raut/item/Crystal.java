package io.gitlab.jfronny.raut.item;

import io.gitlab.jfronny.raut.RaUt;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Crystal extends Item {
    private static final List<StatusEffect> positiveEffects;
    private static final List<StatusEffect> negativeEffects;
    private static final Random rnd;

    static {
        positiveEffects = new ArrayList<>();
        negativeEffects = new ArrayList<>();
        rnd = new Random();
        for (Field field : StatusEffects.class.getFields()) {
            if (StatusEffect.class.equals(field.getType())) {
                Object effect = null;
                try {
                    effect = field.get(null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (effect instanceof StatusEffect) {
                    boolean added = false;
                    StatusEffect tmp = (StatusEffect) effect;
                    for (Method method : StatusEffect.class.getMethods()) {
                        if (method.getName().equals("isBeneficial")) {
                            added = true;
                            try {
                                if ((boolean) method.invoke(tmp))
                                    positiveEffects.add((StatusEffect) effect);
                                else
                                    negativeEffects.add((StatusEffect) effect);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (!added)
                        positiveEffects.add((StatusEffect) effect);
                }
            }
        }
    }

    public Crystal() {
        super(new Settings().group(ItemGroup.FOOD).food(new FoodComponent.Builder().hunger(RaUt.cfg.crystalPlant.crystal.hunger).saturationModifier(RaUt.cfg.crystalPlant.crystal.saturationModifier).alwaysEdible().build()));
    }

    @Override
    public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {
        ItemStack itemStack = super.finishUsing(stack, world, user);
        if (!world.isClient) {
            if (!(user.getActiveStatusEffects().keySet().containsAll(positiveEffects)
                    && user.getActiveStatusEffects().keySet().containsAll(positiveEffects))) {
                List<StatusEffect> effects = !user.getActiveStatusEffects().keySet().containsAll(negativeEffects)
                        && (user.getActiveStatusEffects().keySet().containsAll(positiveEffects)
                        || rnd.nextInt(RaUt.cfg.crystalPlant.crystal.negativeChanceInvert) == 0) ? negativeEffects : positiveEffects;
                if (!user.getActiveStatusEffects().keySet().containsAll(effects)) {
                    List<StatusEffect> eff = new ArrayList<>(effects);
                    eff.removeAll(user.getActiveStatusEffects().keySet());
                    StatusEffect effect = eff.get(rnd.nextInt(eff.size()));
                    if (effect.isInstant())
                        effect.applyInstantEffect(user, user, user, RaUt.cfg.crystalPlant.crystal.effectAmplifier, 1);
                    else
                        user.addStatusEffect(new StatusEffectInstance(effect, RaUt.cfg.crystalPlant.crystal.effectDuration, RaUt.cfg.crystalPlant.crystal.effectAmplifier));
                }
            }
            if (user instanceof PlayerEntity) {
                ((PlayerEntity) user).getItemCooldownManager().set(this, RaUt.cfg.crystalPlant.crystal.cooldown);
            }
        }
        return itemStack;
    }
}
