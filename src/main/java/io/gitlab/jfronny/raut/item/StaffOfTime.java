package io.gitlab.jfronny.raut.item;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.modules.MiscModule;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.server.world.ServerTickScheduler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockBox;
import net.minecraft.world.ScheduledTick;
import net.minecraft.world.World;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Random;

public class StaffOfTime extends Item {
    static final Random rnd = new Random();
    static final Field tickTime;

    static {
        try {
            tickTime = FieldUtils.getField(ScheduledTick.class,
                    FabricLoader.getInstance().getMappingResolver().mapFieldName("intermediary",
                            "net.minecraft.class_1954",
                            "field_9321",
                            "J"),
                    true);
        } catch (Exception e) {
            RaUt.warn("Could not initialize field, will not continue");
            throw new IllegalStateException(e);
        }
    }

    public StaffOfTime() {
        super(new Settings().group(ItemGroup.TOOLS).maxCount(1).maxDamage(RaUt.cfg.misc.staffOfTime.maxDamage));
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        if (!world.isClient) {
            BlockState block = context.getWorld().getBlockState(context.getBlockPos());
            if (block.isIn(MiscModule.STAFF_OF_TIME_ILLEGAL)) {
                return ActionResult.CONSUME;
            }
            try {
                ServerWorld sWorld;
                sWorld = Objects.requireNonNull(world.getServer()).getWorld(world.getRegistryKey());
                if (block.hasRandomTicks())
                    for (int i = 0; i < RaUt.cfg.misc.staffOfTime.speed / RaUt.cfg.misc.staffOfTime.randomTickDiv; i++)
                        block.randomTick(sWorld, context.getBlockPos(), rnd);
                if (world.getBlockTickScheduler().isScheduled(context.getBlockPos(), block.getBlock())) {
                    ServerTickScheduler<Block> scheduler = (ServerTickScheduler<Block>) world.getBlockTickScheduler();
                    for (ScheduledTick<Block> tick : scheduler.getScheduledTicks(new BlockBox(context.getBlockPos()), false, false)) {
                        long timediff = tick.time - world.getTime();
                        timediff -= RaUt.cfg.misc.staffOfTime.speed;
                        tickTime.set(tick, world.getTime() + timediff);
                    }
                }
                //TODO this is no longer used, look in gwwhit for how this is done now
                /*BlockEntity e = world.getBlockEntity(context.getBlockPos());
                if (e instanceof Tickable) {
                    for (int i = 0; i < RaUt.cfg.misc.staffOfTime.speed; i++)
                        ((Tickable) e).tick();
                }*/
            } catch (NullPointerException | IllegalAccessException ex) {
                RaUt.warn("Caught Exception while running tick:");
                ex.printStackTrace();
            }
            PlayerEntity player = Objects.requireNonNull(context.getPlayer());
            if (!player.getAbilities().creativeMode) {
                context.getStack().damage(1, player, (p) -> p.sendToolBreakStatus(player.getActiveHand()));
            }
        }
        return ActionResult.SUCCESS;
    }
}
