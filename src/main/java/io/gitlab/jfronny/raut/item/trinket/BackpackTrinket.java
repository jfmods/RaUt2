package io.gitlab.jfronny.raut.item.trinket;

import io.gitlab.jfronny.raut.gui.BackpackGuiFactory;
import io.gitlab.jfronny.raut.gui.BackpackInventory;
import dev.emi.trinkets.api.TrinketItem;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import java.util.List;

public class BackpackTrinket extends TrinketItem {
    public enum Size {
        Small,
        Medium,
        Large
    }

    private final Size size;

    public Size getSize() {
        return size;
    }

    public BackpackTrinket(Size size) {
        super(new Settings().group(ItemGroup.MISC).maxCount(1));
        this.size = size;
    }

    public void openGUI(ItemStack stack, PlayerEntity player) {
        if (!stack.isEmpty() && stack.getItem() instanceof BackpackTrinket) {
            if (!stack.hasTag()) {
                stack.setTag(new NbtCompound());
            }

            player.openHandledScreen(new BackpackGuiFactory(stack, ((BackpackTrinket) stack.getItem()).size));
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity playerEntity, Hand hand) {
        if (world.isClient) return new TypedActionResult<>(ActionResult.PASS, playerEntity.getStackInHand(hand));

        ItemStack stack = playerEntity.getStackInHand(hand);
        openGUI(stack, playerEntity);

        return new TypedActionResult<>(ActionResult.SUCCESS, stack);
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        tooltip.add(new TranslatableText ("item.raut.backpack.tooltip." + size.name().toLowerCase()));
    }

    //TODO reimplement
    /*@Override
    @Environment(EnvType.CLIENT)
    public void render(String slot, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light, PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player, float headYaw, float headPitch) {
        ItemRenderer itemRenderer = MinecraftClient.getInstance().getItemRenderer();
        ItemStack stack = new ItemStack(this);
        //Translate to chest
        if (player.isInSneakingPose() && !model.riding && !player.isSwimming()) {
            matrixStack.translate(0.0F, 0.2F, 0.0F);
            matrixStack.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(model.body.pitch * 57.5F));
        }
        matrixStack.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(model.body.yaw * 57.5F));
        matrixStack.translate(0.0F, 0.4F, -0.16F);
        //Translate backwards if wearing chestplate
        boolean hasChestplate = false;
        for (ItemStack item : player.getArmorItems()) {
            Item tmpItem = item.getItem();
            if ((tmpItem instanceof ArmorItem) && ((ArmorItem) tmpItem).getSlotType() == EquipmentSlot.CHEST) {
                hasChestplate = true;
            }
        }
        if (hasChestplate) {
            matrixStack.translate(0, 0, 0.05);
        }
        //Actual rendering
        matrixStack.translate(0, 0.05, 0.3);
        itemRenderer.renderItem(stack, ModelTransformation.Mode.FIXED, light, 0, matrixStack, vertexConsumer);
        matrixStack.translate(0, -0.05, -0.3);
        //Undo chestplate transformation
        if (hasChestplate) {
            matrixStack.translate(0, 0, -0.05);
        }
        //Undo translation to chest
        matrixStack.translate(0.0F, -0.4F, 0.16F);
        matrixStack.multiply(Vec3f.NEGATIVE_Y.getDegreesQuaternion(model.body.yaw * 57.5F));
        if (player.isInSneakingPose() && !model.riding && !player.isSwimming()) {
            matrixStack.multiply(Vec3f.NEGATIVE_X.getDegreesQuaternion(model.body.pitch * 57.5F));
            matrixStack.translate(0.0F, -0.2F, 0.0F);
        }
    }*/
}
