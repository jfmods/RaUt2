package io.gitlab.jfronny.raut.item.armor;

import io.gitlab.jfronny.raut.util.AttributeArmorMat;
import io.gitlab.jfronny.raut.modules.AquiloriteModule;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import org.apache.commons.lang3.tuple.Triple;

import java.util.HashMap;

public class AquiloriteArmorMat extends AttributeArmorMat {
    private static final int[] PROTECTION_AMOUNTS = new int[]{3, 6, 8, 3};

    @Override
    public int getDurability(EquipmentSlot slot) {
        return BASE_DURABILITY[slot.getEntitySlotId()] * 40;
    }

    @Override
    public int getProtectionAmount(EquipmentSlot slot) {
        return PROTECTION_AMOUNTS[slot.getEntitySlotId()];
    }

    @Override
    public int getEnchantability() {
        return 30;
    }

    @Override
    public SoundEvent getEquipSound() {
        return SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.ofItems(AquiloriteModule.AQUILORITE_GEM);
    }

    @Override
    public String getName() {
        return "aquilorite";
    }

    @Override
    public float getToughness() {
        return 2.5f;
    }

    @Override
    public float getKnockbackResistance() {
        return 0.1f;
    }

    @Override
    public HashMap<EquipmentSlot, HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>>> getAttributes() {
        HashMap<EquipmentSlot, HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>>> map = new HashMap<>();
        HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>> legs = new HashMap<>();
        legs.put(EntityAttributes.GENERIC_MOVEMENT_SPEED, getTr("Movement Speed", 0.05));
        legs.put(EntityAttributes.GENERIC_FLYING_SPEED, getTr("Flying Speed", 0.01));
        map.put(EquipmentSlot.LEGS, legs);
        HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>> chest = new HashMap<>();
        chest.put(EntityAttributes.GENERIC_ARMOR_TOUGHNESS, getTr("Resistance", 5d));
        map.put(EquipmentSlot.CHEST, chest);
        HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>> feet = new HashMap<>();
        feet.put(EntityAttributes.GENERIC_KNOCKBACK_RESISTANCE, getTr("Knockback Resistance", 0.1));
        map.put(EquipmentSlot.FEET, feet);
        HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>> head = new HashMap<>();
        head.put(EntityAttributes.GENERIC_LUCK, getTr("Luck", 2d));
        head.put(EntityAttributes.GENERIC_ATTACK_SPEED, getTr("Attack Speed", 0.1));
        map.put(EquipmentSlot.HEAD, head);
        return map;
    }

    private Triple<String, Double, EntityAttributeModifier.Operation> getTr(String name, Double add) {
        return new Triple<>() {
            @Override
            public String getLeft() {
                return name;
            }

            @Override
            public Double getMiddle() {
                return add;
            }

            @Override
            public EntityAttributeModifier.Operation getRight() {
                return EntityAttributeModifier.Operation.ADDITION;
            }
        };
    }
}
