package io.gitlab.jfronny.raut.item.trinket;

import dev.emi.trinkets.api.SlotReference;
import io.gitlab.jfronny.raut.RaUt;
import dev.emi.trinkets.api.TrinketItem;
import io.github.ladysnake.pal.AbilitySource;
import io.github.ladysnake.pal.Pal;
import io.github.ladysnake.pal.VanillaAbilities;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;

public class AngelRing extends TrinketItem {
    private final AbilitySource abilitySource = Pal.getAbilitySource(RaUt.MOD_ID, "angel_ring");

    public AngelRing() {
        super(new Settings().group(ItemGroup.TOOLS).maxCount(1));
    }

    @Environment(EnvType.CLIENT)
    public void appendTooltip(ItemStack stack, World world, List<Text> list, TooltipContext context) {
        list.add(new LiteralText("Flight").formatted(Formatting.GOLD));
    }

    //TODO reimplement
    /*@Override
    @Environment(EnvType.CLIENT)
    public void render(String slot, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light, PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player, float headYaw, float headPitch) {
        RingRenderer.Render(this, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch);
    }*/

    @Override
    public void onEquip(ItemStack stack, SlotReference slot, LivingEntity entity) {
        if (!entity.world.isClient && entity instanceof PlayerEntity player && !abilitySource.grants(player, VanillaAbilities.ALLOW_FLYING)) {
            abilitySource.grantTo(player, VanillaAbilities.ALLOW_FLYING);
        }
    }

    @Override
    public void onUnequip(ItemStack stack, SlotReference slot, LivingEntity entity) {
        if (!entity.world.isClient && entity instanceof PlayerEntity player && abilitySource.grants(player, VanillaAbilities.ALLOW_FLYING)) {
            abilitySource.revokeFrom(player, VanillaAbilities.ALLOW_FLYING);
        }
    }
}
