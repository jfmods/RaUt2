package io.gitlab.jfronny.raut.item.trinket;

import com.google.common.collect.Multimap;
import dev.emi.stepheightentityattribute.StepHeightEntityAttributeMain;
import dev.emi.trinkets.api.SlotReference;
import io.gitlab.jfronny.raut.RaUt;
import dev.emi.trinkets.api.TrinketItem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;
import java.util.UUID;

public class TravellersRing extends TrinketItem {
    public TravellersRing() {
        super(new Settings().group(ItemGroup.TOOLS).maxCount(1));
    }

    @Environment(EnvType.CLIENT)
    public void appendTooltip(ItemStack stack, World world, List<Text> list, TooltipContext context) {
        list.add(new LiteralText("Swifter movement").formatted(Formatting.GOLD));
    }

    //TODO reimplement
    /*@Override
    public boolean canWearInSlot(String group, String slot) {
        return group.equals(SlotGroups.HAND) && slot.equals(Slots.RING);
    }

    @Override
    public Multimap<EntityAttribute, EntityAttributeModifier> getTrinketModifiers(String group, String slot, UUID uuid, ItemStack stack) {
        Multimap<EntityAttribute, EntityAttributeModifier> map = HashMultimap.create();
        if (slot.equals(Slots.RING)) {
            map.put(StepHeightEntityAttributeMain.STEP_HEIGHT, new EntityAttributeModifier(uuid, "Step Height", RaUt.cfg.trinkets.traveller_ring.addedStepHeight, EntityAttributeModifier.Operation.ADDITION));
            map.put(EntityAttributes.GENERIC_MOVEMENT_SPEED, new EntityAttributeModifier(uuid, "Movement Speed", RaUt.cfg.trinkets.traveller_ring.movementSpeedFactor, EntityAttributeModifier.Operation.MULTIPLY_TOTAL));
        }
        return map;
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void render(String slot, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light, PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player, float headYaw, float headPitch) {
        RingRenderer.Render(this, matrixStack, vertexConsumer, light, model, player, headYaw, headPitch);
    }*/

    @Override
    public Multimap<EntityAttribute, EntityAttributeModifier> getModifiers(ItemStack stack, SlotReference slot, LivingEntity entity, UUID uuid) {
        var modifiers = super.getModifiers(stack, slot, entity, uuid);
        modifiers.put(StepHeightEntityAttributeMain.STEP_HEIGHT, new EntityAttributeModifier(uuid, "Step Height", RaUt.cfg.trinkets.traveller_ring.addedStepHeight, EntityAttributeModifier.Operation.ADDITION));
        modifiers.put(EntityAttributes.GENERIC_MOVEMENT_SPEED, new EntityAttributeModifier(uuid, "Movement Speed", RaUt.cfg.trinkets.traveller_ring.movementSpeedFactor, EntityAttributeModifier.Operation.MULTIPLY_TOTAL));
        return modifiers;
    }
}
