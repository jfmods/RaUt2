package io.gitlab.jfronny.raut.item;

import io.gitlab.jfronny.raut.RaUt;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.world.World;

public class GunProjectile extends ArrowEntity {
    public GunProjectile(World world, LivingEntity owner) {
        super(world, owner);
    }

    @Override
    protected void onHit(LivingEntity target) {
        super.onHit(target);
        if (RaUt.cfg.aquilorite.gun.overpowered)
            target.kill();
    }
}
