package io.gitlab.jfronny.raut.item;

import io.gitlab.jfronny.raut.modules.AquiloriteModule;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.RangedWeaponItem;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.UseAction;
import net.minecraft.world.World;

import java.util.Random;
import java.util.function.Predicate;

public class AquiloriteGun extends RangedWeaponItem {
    private static final Random RANDOM = new Random();
    public AquiloriteGun() {
        super(new Settings().group(ItemGroup.COMBAT).maxDamage(2000));
    }

    @Override
    public Predicate<ItemStack> getProjectiles() {
        return stack -> stack.getItem().equals(AquiloriteModule.EXPLOSIVE_SHARD);
    }

    @Override
    public int getRange() {
        return 0;
    }

    public UseAction getUseAction(ItemStack stack) {
        return UseAction.BOW;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        ItemStack arrows = user.getArrowType(stack);
        boolean inf = user.getAbilities().creativeMode || EnchantmentHelper.getLevel(Enchantments.INFINITY, stack) > 0;
        if (!arrows.isEmpty() || inf) {
            if (arrows.isEmpty())
                arrows = new ItemStack(AquiloriteModule.EXPLOSIVE_SHARD);
            if (!world.isClient) {
                //ArrowItem arrowItem = (ArrowItem) (arrows.getItem() instanceof ArrowItem ? arrows.getItem() : AquiloriteModule.EXPLOSIVE_SHARD);
                //PersistentProjectileEntity projectileEntity = arrowItem.createArrow(world, arrows, user);
                GunProjectile projectileEntity = new GunProjectile(world, user);
                projectileEntity.initFromStack(arrows);
                projectileEntity.setProperties(user, user.getPitch(), user.getYaw(), 0.0F, 3, 1.0F);
                projectileEntity.setDamage(projectileEntity.getDamage() * 2);
                stack.damage(1, user, (p) -> p.sendToolBreakStatus(user.getActiveHand()));
                projectileEntity.pickupType = PersistentProjectileEntity.PickupPermission.CREATIVE_ONLY;
                world.spawnEntity(projectileEntity);
            }
            world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.BLOCK_ANVIL_BREAK, SoundCategory.PLAYERS, 1.0F, 1.0F / (RANDOM.nextFloat() * 0.4F + 1.2F) + 0.5F);
            if (!inf && !user.getAbilities().creativeMode) {
                arrows.decrement(1);
                if (arrows.isEmpty()) {
                    user.getInventory().removeOne(arrows);
                }
            }
        }
        return TypedActionResult.pass(stack);
    }
}
