package io.gitlab.jfronny.raut.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;

import static io.gitlab.jfronny.raut.modules.MiscModule.COBBLE_GENERATOR_ENTITY;

public class CobbleGeneratorEntity extends BlockEntity {
    public int cobbleCount = 0;

    public CobbleGeneratorEntity(BlockPos pos, BlockState state) {
        super(COBBLE_GENERATOR_ENTITY, pos, state);
    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);
        cobbleCount = tag.getInt("c");
    }

    @Override
    public NbtCompound writeNbt(NbtCompound tag) {
        super.writeNbt(tag);
        tag.putInt("c", cobbleCount);
        return tag;
    }
}
