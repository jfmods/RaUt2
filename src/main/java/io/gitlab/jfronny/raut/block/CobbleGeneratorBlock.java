package io.gitlab.jfronny.raut.block;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.util.MiningLevel;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Random;

import static io.gitlab.jfronny.raut.modules.MiscModule.COBBLE_GENERATOR;

public class CobbleGeneratorBlock extends Block implements BlockEntityProvider {
    public CobbleGeneratorBlock() {
        super(FabricBlockSettings.of(Material.STONE).breakByHand(false).breakByTool(FabricToolTags.PICKAXES, MiningLevel.IRON).strength(RaUt.cfg.misc.cobbleGen.hardness, RaUt.cfg.misc.cobbleGen.resistance));
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new CobbleGeneratorEntity(pos, state);
    }

    @Override
    public boolean hasRandomTicks(BlockState state) {
        return true;
    }

    @Override
    public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        if (!world.getBlockTickScheduler().isScheduled(pos, COBBLE_GENERATOR)) {
            world.getBlockTickScheduler().schedule(pos, COBBLE_GENERATOR, RaUt.cfg.misc.cobbleGen.initialDelay);
        }
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        CobbleGeneratorEntity entity = (CobbleGeneratorEntity) world.getBlockEntity(pos);
        if (entity != null && entity.cobbleCount > 0) {
            if (!world.isClient) {
                ItemStack tmp = new ItemStack(Items.COBBLESTONE);
                tmp.setCount(Math.min(entity.cobbleCount, 64));
                entity.cobbleCount -= tmp.getCount();
                entity.markDirty();
                player.giveItemStack(tmp);
            }
            return ActionResult.SUCCESS;
        } else
            return ActionResult.CONSUME;
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        CobbleGeneratorEntity entity = (CobbleGeneratorEntity) world.getBlockEntity(pos);
        if (entity != null && entity.cobbleCount < RaUt.cfg.misc.cobbleGen.maxCobble) {
            entity.cobbleCount++;
            entity.markDirty();
        }
        world.getBlockTickScheduler().schedule(pos, COBBLE_GENERATOR, RaUt.cfg.misc.cobbleGen.runDelay);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
        super.onPlaced(world, pos, state, placer, itemStack);
        world.getBlockTickScheduler().schedule(pos, COBBLE_GENERATOR, RaUt.cfg.misc.cobbleGen.initialDelay);
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        super.onBreak(world, pos, state, player);
        if (!world.isClient()) {
            CobbleGeneratorEntity entity = (CobbleGeneratorEntity) world.getBlockEntity(pos);
            if (entity != null) {
                while (entity.cobbleCount > 0) {
                    ItemStack tmp = new ItemStack(Items.COBBLESTONE);
                    tmp.setCount(Math.min(entity.cobbleCount, 64));
                    entity.cobbleCount -= tmp.getCount();
                    entity.markDirty();
                    ItemEntity itemEntity = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), tmp);
                    world.spawnEntity(itemEntity);
                }
            } else {
                RaUt.info("No blockentity found");
            }
        }
    }
}
