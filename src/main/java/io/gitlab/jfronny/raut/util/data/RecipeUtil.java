package io.gitlab.jfronny.raut.util.data;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecipeUtil {
    private final static Set<String> removalsByIdentifier = new HashSet<>();
    private final static List<ItemStack> recipesForRemoval = new ArrayList<>();

    public static void removeRecipe(String id) {
        removalsByIdentifier.add(id);
    }

    public static void removeRecipeFor(ItemStack product) {
        recipesForRemoval.add(product);
    }

    public static Iterable<ItemStack> getRecipesForRemoval() {
        return recipesForRemoval;
    }

    public static Set<String> getIdentifiersForRemoval() {
        return removalsByIdentifier;
    }
}
