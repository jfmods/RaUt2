package io.gitlab.jfronny.raut.util.data;

import com.google.common.collect.ImmutableSet;
import net.minecraft.util.Identifier;

import java.util.Set;

public interface TagBuilder {
    TagBuilder NONE = new NullTagBuilder();

    TagBuilder AXES = new BasicTagBuilder(new Identifier("fabric", "items/axes"));
    TagBuilder HOES = new BasicTagBuilder(new Identifier("fabric", "items/hoes"));
    TagBuilder PICKAXES = new BasicTagBuilder(new Identifier("fabric", "items/pickaxes"));
    TagBuilder SHEARS = new BasicTagBuilder(new Identifier("fabric", "items/shears"));
    TagBuilder SHOVELS = new BasicTagBuilder(new Identifier("fabric", "items/shovels"));
    TagBuilder SWORDS = new BasicTagBuilder(new Identifier("fabric", "items/swords"));
    TagBuilder PAXELS = new CompoundTagBuilder(ImmutableSet.of(AXES, HOES, PICKAXES, SHEARS, SHOVELS, SWORDS));

    TagBuilder STEEL_BLOCKS = new CompoundTagBuilder(ImmutableSet.of(new BasicTagBuilder(new Identifier("c", "blocks/steel_blocks")), new BasicTagBuilder(new Identifier("c", "items/steel_blocks"))));
    TagBuilder GOLD_DUSTS = new BasicTagBuilder(new Identifier("c", "items/gold_dusts"));
    TagBuilder IRON_DUSTS = new BasicTagBuilder(new Identifier("c", "items/iron_dusts"));
    TagBuilder PLANTABLES = new BasicTagBuilder(new Identifier("c", "items/plantables"));
    TagBuilder STEEL_INGOTS = new BasicTagBuilder(new Identifier("c", "items/steel_ingots"));
    TagBuilder STEEL_NUGGETS = new BasicTagBuilder(new Identifier("c", "items/steel_nuggets"));

    TagBuilder SHULKER_BOXES_ILLEGAL = new BasicTagBuilder(new Identifier("raut", "items/shulker_boxes_illegal"));
    TagBuilder AQUILORITE_ARMOR = new BasicTagBuilder(new Identifier("raut", "items/aquilorite_armor"));
    TagBuilder CHAIN_PLATE = new BasicTagBuilder(new Identifier("raut", "items/chain_plate"));
    TagBuilder AQUILORITE_GEM = new BasicTagBuilder(new Identifier("raut", "items/aquilorite_gem"));

    // compound tags don't need subtags to be specified separately
    Set<TagBuilder> TAG_BUILDERS = ImmutableSet.of(NONE, PAXELS, STEEL_BLOCKS, GOLD_DUSTS, IRON_DUSTS, PLANTABLES, STEEL_INGOTS, STEEL_NUGGETS, SHULKER_BOXES_ILLEGAL, AQUILORITE_ARMOR, CHAIN_PLATE, AQUILORITE_GEM);

    Identifier add(Identifier entry, boolean condition);
    String add(String rautItem, boolean condition);
    void apply();
}
