package io.gitlab.jfronny.raut.util;

import io.gitlab.jfronny.raut.RaUt;
import io.gitlab.jfronny.raut.util.basicItem.*;
import io.gitlab.jfronny.raut.util.data.TagBuilder;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.List;

import static io.gitlab.jfronny.raut.RaUt.MOD_ID;
import static io.gitlab.jfronny.raut.util.data.TagBuilder.*;

public class DepRegistry {
    public static HashMap<String, Item> disabledItems = new HashMap<>();

    public static <T extends Item> T registerItem(String ID, Boolean dep, T item) {
        if (dep) {
            try {
                Registry.register(Registry.ITEM, new Identifier(MOD_ID, ID), item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (RaUt.cfg.safeDisable) {
                Item nullIt = new DisabledItem();
                disabledItems.put(ID, nullIt);
                Registry.register(Registry.ITEM, new Identifier(MOD_ID, ID), nullIt);
            }
        }
        return item;
    }

    public static void registerBlock(String ID, Boolean dep, Block block) {
        registerBlock(ID, dep, block, ItemGroup.BUILDING_BLOCKS);
    }

    public static void registerBlock(String ID, Boolean dep, Block block, ItemGroup group) {
        registerBlock(ID, dep, block, new BlockItem(block, new Item.Settings().group(group)));
    }

    public static void registerBlock(String ID, Boolean dep, Block block, BlockItem item) {
        try {
            if (dep)
                Registry.register(Registry.BLOCK, new Identifier(MOD_ID, ID), block);
            else if (RaUt.cfg.safeDisable)
                Registry.register(Registry.BLOCK, new Identifier(MOD_ID, ID), new Block(FabricBlockSettings.of(Material.AIR)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        registerItem(ID, dep, item);
    }

    public static void registerTools(String IDPrefix, Boolean dep, FactorToolMat mat) {
        registerItem(PAXELS.add(IDPrefix + "_paxel", dep), dep, new BasePaxel(mat, (int) mat.getPickaxeDamage(),
                registerItem(AXES.add(IDPrefix + "_axe", dep), dep, new BaseAxe(mat)),
                registerItem(SWORDS.add(IDPrefix + "_sword", dep), dep, new BaseSword(mat)),
                registerItem(PICKAXES.add(IDPrefix + "_pickaxe", dep), dep, new BasePickaxe(mat)),
                registerItem(SHOVELS.add(IDPrefix + "_shovel", dep), dep, new BaseShovel(mat)),
                registerItem(HOES.add(IDPrefix + "_hoe", dep), dep, new BaseHoe(mat))));
    }

    public static void registerArmor(String IDPrefix, Boolean dep, AttributeArmorMat mat, TagBuilder tag) {
        registerItem(tag.add(IDPrefix + "_helmet", dep), dep, new BaseArmor(mat, EquipmentSlot.HEAD));
        registerItem(tag.add(IDPrefix + "_chestplate", dep), dep, new BaseArmor(mat, EquipmentSlot.CHEST));
        registerItem(tag.add(IDPrefix + "_leggings", dep), dep, new BaseArmor(mat, EquipmentSlot.LEGS));
        registerItem(tag.add(IDPrefix + "_boots", dep), dep, new BaseArmor(mat, EquipmentSlot.FEET));
    }

    public static class DisabledItem extends Item {
        public DisabledItem() {
            super(new Settings());
        }

        @Override
        public void appendTooltip(ItemStack itemStack, World world, List<Text> tooltip, TooltipContext tooltipContext) {
            tooltip.add(new TranslatableText("status.raut.disabled"));
        }
    }
}
