package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.FactorToolMat;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemGroup;

public class BaseHoe extends HoeItem {
    public BaseHoe(FactorToolMat material) {
        super(material, (int) (material.getAttackDamage() / 2), material.getHoeSpeed() - 4, new Settings().group(ItemGroup.TOOLS));
    }
}
