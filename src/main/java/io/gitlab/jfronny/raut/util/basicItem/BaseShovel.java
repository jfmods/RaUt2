package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.FactorToolMat;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ShovelItem;

public class BaseShovel extends ShovelItem {
    public BaseShovel(FactorToolMat material) {
        super(material, material.getShovelDamage() - 1 - material.getAttackDamage(), -3, new Settings().group(ItemGroup.TOOLS));
    }
}
