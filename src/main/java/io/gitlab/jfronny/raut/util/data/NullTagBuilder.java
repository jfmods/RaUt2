package io.gitlab.jfronny.raut.util.data;

import net.minecraft.util.Identifier;

public class NullTagBuilder implements TagBuilder {
    @Override
    public Identifier add(Identifier entry, boolean condition) {
        return entry;
    }

    @Override
    public String add(String rautItem, boolean condition) {
        return rautItem;
    }

    @Override
    public void apply() {

    }
}
