package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.basicItem.PaxelItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.*;

import java.util.function.Supplier;

public class BasePaxel extends PaxelItem {
    Supplier<Boolean> killInstantly = () -> false;

    public BasePaxel(ToolMaterial material, int attackDamage, AxeItem baseAxe, SwordItem baseSword, PickaxeItem basePickaxe, ShovelItem baseShovel, HoeItem baseHoe) {
        super(material, attackDamage, new Settings().group(ItemGroup.TOOLS).maxDamage(material.getDurability() * 3),
                baseAxe, baseSword, basePickaxe, baseShovel, baseHoe);
    }

    public BasePaxel(ToolMaterial material, int attackDamage, AxeItem baseAxe, SwordItem baseSword, PickaxeItem basePickaxe, ShovelItem baseShovel, HoeItem baseHoe, Supplier<Boolean> killInstantly) {
        super(material, attackDamage, new Settings().group(ItemGroup.TOOLS).maxDamage(material.getDurability() * 3),
                baseAxe, baseSword, basePickaxe, baseShovel, baseHoe);
        this.killInstantly = killInstantly;
    }

    @Override
    public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
        if (killInstantly.get()) {
            target.kill();
        }
        return super.postHit(stack, target, attacker);
    }
}
