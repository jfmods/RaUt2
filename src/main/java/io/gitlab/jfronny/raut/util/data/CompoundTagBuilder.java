package io.gitlab.jfronny.raut.util.data;

import net.minecraft.util.Identifier;

import java.util.Set;

public record CompoundTagBuilder(Set<TagBuilder> builderSet) implements TagBuilder {
    @Override
    public Identifier add(Identifier entry, boolean condition) {
        for (TagBuilder builder : builderSet) {
            builder.add(entry, condition);
        }
        return entry;
    }

    @Override
    public String add(String rautItem, boolean condition) {
        for (TagBuilder builder : builderSet) {
            builder.add(rautItem, condition);
        }
        return rautItem;
    }

    @Override
    public void apply() {
        for (TagBuilder builder : builderSet) {
            builder.apply();
        }
    }
}
