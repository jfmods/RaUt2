package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.FactorToolMat;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SwordItem;

public class BaseSword extends SwordItem {
    public BaseSword(FactorToolMat material) {
        super(material, (int) (material.getSwordDamage() - 1 - material.getAttackDamage()), -3, new Settings().group(ItemGroup.COMBAT));
    }
}
