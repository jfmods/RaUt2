package io.gitlab.jfronny.raut.util;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.model.PlayerEntityModel;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3f;

public class RingRenderer {
    public static void render(Item ring, MatrixStack matrixStack, VertexConsumerProvider vertexConsumer, int light, PlayerEntityModel<AbstractClientPlayerEntity> model, AbstractClientPlayerEntity player, float headYaw, float headPitch) {
        ItemRenderer itemRenderer = MinecraftClient.getInstance().getItemRenderer();
        ItemStack stack = new ItemStack(ring);
        //Translate to chest
        if (player.isInSneakingPose() && !model.riding && !player.isSwimming()) {
            matrixStack.translate(0.0F, 0.2F, 0.0F);
            matrixStack.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(model.body.pitch * 57.5F));
        }
        matrixStack.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(model.body.yaw * 57.5F));
        matrixStack.translate(0.0F, 0.4F, -0.13F);
        //
        boolean hasChestplate = false;
        for (ItemStack item : player.getArmorItems()) {
            Item tmpItem = item.getItem();
            if ((tmpItem instanceof ArmorItem) && ((ArmorItem) tmpItem).getSlotType() == EquipmentSlot.CHEST) {
                hasChestplate = true;
            }
        }
        if (hasChestplate) {
            matrixStack.translate(0, 0, -0.07);
        }
        matrixStack.scale(0.5f, -0.5f, -0.5f);
        matrixStack.translate(0, 0.2, 0);
        itemRenderer.renderItem(stack, ModelTransformation.Mode.FIXED, light, 0, matrixStack, vertexConsumer, 0);
        matrixStack.translate(0, -0.2, 0);
        matrixStack.scale(2f, -2f, -2f);
        if (hasChestplate) {
            matrixStack.translate(0, 0, 0.05);
        }
        //Undo translation to chest
        matrixStack.translate(0.0F, -0.4F, 0.13F);
        matrixStack.multiply(Vec3f.NEGATIVE_Y.getDegreesQuaternion(model.body.yaw * 57.5F));
        if (player.isInSneakingPose() && !model.riding && !player.isSwimming()) {
            matrixStack.multiply(Vec3f.NEGATIVE_X.getDegreesQuaternion(model.body.pitch * 57.5F));
            matrixStack.translate(0.0F, -0.2F, 0.0F);
        }
    }
}
