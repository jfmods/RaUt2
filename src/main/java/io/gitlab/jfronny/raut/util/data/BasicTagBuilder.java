package io.gitlab.jfronny.raut.util.data;

import io.gitlab.jfronny.raut.RaUt;
import net.devtech.arrp.json.tags.JTag;
import net.minecraft.util.Identifier;

public class BasicTagBuilder implements TagBuilder {
    private final Identifier id;
    private final JTag tag;

    public BasicTagBuilder(Identifier id) {
        this.id = id;
        this.tag = JTag.tag();
    }

    @Override
    public void apply() {
        RaUt.DATA_PACK.addTag(id, tag);
    }

    @Override
    public Identifier add(Identifier entry, boolean condition) {
        if (condition)
            tag.add(entry);
        return entry;
    }

    @Override
    public String add(String rautItem, boolean condition) {
        add(new Identifier(RaUt.MOD_ID, rautItem), condition);
        return rautItem;
    }
}
