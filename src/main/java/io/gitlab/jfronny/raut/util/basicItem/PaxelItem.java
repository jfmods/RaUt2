package io.gitlab.jfronny.raut.util.basicItem;

import net.minecraft.block.BlockState;
import net.minecraft.item.*;
import net.minecraft.util.ActionResult;

public class PaxelItem extends PickaxeItem {
    ToolMaterial material;
    AxeItem baseAxe;
    SwordItem baseSword;
    PickaxeItem basePickaxe;
    ShovelItem baseShovel;
    HoeItem baseHoe;

    public PaxelItem(ToolMaterial material, int attackDamage, Settings settings, AxeItem baseAxe, SwordItem baseSword, PickaxeItem basePickaxe, ShovelItem baseShovel, HoeItem baseHoe) {
        super(material, (int) (attackDamage - 1 - material.getAttackDamage()), -2.8f, settings); //effectiveBlocks shouldn't be used anywhere since getMiningSpeedMultiplier is overridden
        this.material = material;
        this.baseAxe = baseAxe;
        this.baseSword = baseSword;
        this.basePickaxe = basePickaxe;
        this.baseShovel = baseShovel;
        this.baseHoe = baseHoe;
    }

    @Override
    public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
        return material.getMiningSpeedMultiplier();
    }

    @Override
    public boolean isSuitableFor(BlockState state) {
        return baseAxe.isSuitableFor(state) || baseSword.isSuitableFor(state) || basePickaxe.isSuitableFor(state) || baseShovel.isSuitableFor(state) || baseHoe.isSuitableFor(state);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext ctx) {
        ActionResult tmp = baseAxe.useOnBlock(ctx);
        if (tmp == ActionResult.PASS) {
            tmp = baseShovel.useOnBlock(ctx);
        }
        if (tmp == ActionResult.PASS) {
            tmp = baseHoe.useOnBlock(ctx);
        }
        if (tmp == ActionResult.PASS) {
            tmp = basePickaxe.useOnBlock(ctx);
        }
        if (tmp == ActionResult.PASS) {
            tmp = baseSword.useOnBlock(ctx);
        }
        return tmp;
    }
}
