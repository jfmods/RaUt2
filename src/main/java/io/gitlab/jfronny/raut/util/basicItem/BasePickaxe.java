package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.FactorToolMat;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.PickaxeItem;

public class BasePickaxe extends PickaxeItem {
    public BasePickaxe(FactorToolMat material) {
        super(material, (int) (material.getPickaxeDamage() - 1 - material.getAttackDamage()), -2.8f, new Settings().group(ItemGroup.TOOLS));
    }
}
