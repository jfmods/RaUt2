package io.gitlab.jfronny.raut.util.basicItem;

import io.gitlab.jfronny.raut.util.FactorToolMat;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemGroup;

public class BaseAxe extends AxeItem {
    public BaseAxe(FactorToolMat material) {
        super(material, material.getAxeDamage() - 1 - material.getAttackDamage(), -3, new Settings().group(ItemGroup.TOOLS));
    }
}
