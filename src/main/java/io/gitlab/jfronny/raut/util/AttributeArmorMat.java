package io.gitlab.jfronny.raut.util;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.ArmorMaterial;
import org.apache.commons.lang3.tuple.Triple;

import java.util.HashMap;

public abstract class AttributeArmorMat implements ArmorMaterial {
    public abstract HashMap<EquipmentSlot, HashMap<EntityAttribute, Triple<String, Double, EntityAttributeModifier.Operation>>> getAttributes();

    protected static final int[] BASE_DURABILITY = new int[]{13, 15, 16, 11};
}
